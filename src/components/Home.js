import React, {Component, Fragment} from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Login from './Login';
import SignupStudent from './SignupStudent';
import SignupAdmin from './SignupAdmin';
import Unauthorise from './Unauthorise';
import Dashboard from './Dashboard';

import { Row, Column} from 'simple-flexbox';
import HeaderComponent from './HeaderComponent';
import SidebarComponent from './SidebarComponent';
import Projects from './Projects';
import Admins from './Admins';
import Agents from './Agents';
import Students from './Students';
import ApplicantForm from './ApplicantForm';
import StaffMapping from './StaffMapping';
import PageNotFound from './PageNotFound';

class Home extends Component{
    constructor(props){
        super(props);
        this.state = {
            searchText: '',
            emailToNavigate: '',
            selectedMenu: '',
            isAuthSucess: false
        }
        this.setTitle = this.setTitle.bind(this);
        this.setSearchText = this.setSearchText.bind(this);
        this.navigateToApplication = this.navigateToApplication.bind(this);
        this.isAuthenticated = this.isAuthenticated.bind(this);
    }

    setSearchText = searchText => {
        this.setState({
            searchText: searchText
        })
    }

    setTitle = title => {
        this.setState({
            selectedMenu: title
        })
    }

    navigateToApplication = email => {
        this.setState({
            emailToNavigate: email,
            selectedMenu: 'Application'
        })
    }

    isAuthenticated = authSuceess => {
        this.setState({
            isAuthSucess: authSuceess
        })
    }

    render() {
        const isAdminUser = localStorage.getItem('access_role') === 'SUPER_ADMIN' || localStorage.getItem('access_role') === 'ADMIN';
        const isAuthenticateSucess = this.state.isAuthSucess;
        const accessRoleFound = localStorage.getItem('access_role') !== null && localStorage.getItem('access_role') !== undefined 
                                    && localStorage.getItem('access_role') !== '';
        const accessAllowed = isAuthenticateSucess || accessRoleFound;
        return (
            <Fragment>
                <Router>
                    <switch>
                        <Route path={`${process.env.PUBLIC_URL}/`} exact render= { props => (<Login isAuthenticated={this.isAuthenticated} {... props} />)} />
                        <Route path={`${process.env.PUBLIC_URL}/signUpStudent`} exact render= { props => (<SignupStudent {... props} />)} />                            
                        <Route path={`${process.env.PUBLIC_URL}/signUpAdmin`} exact render= { props => (<SignupAdmin {... props} /> )} />
                        <Route path={`${process.env.PUBLIC_URL}/unAuthorise`} exact render= { props => (<Unauthorise {... props} />)} />

                        <Route path={`${process.env.PUBLIC_URL}/home`}>
                            { accessAllowed && 
                                <Row className="homeContainer">
                                    <SidebarComponent />
                                    <Column flexGrow={1} className="homeMainBlock">
                                        <HeaderComponent title={this.state.selectedMenu} setSearchText={this.setSearchText}/>
                                        <div className="m-t-20" >
                                            <Route path={`${process.env.PUBLIC_URL}/home/dashboard`} exact 
                                                render= { props => (<Dashboard setTitle={this.setTitle} searchText={this.state.searchText} {... props} /> )} />

                                            { isAdminUser && <Route path={`${process.env.PUBLIC_URL}/home/subAgency`} exact 
                                                render= { props => (<Projects setTitle={this.setTitle} searchText={this.state.searchText} {... props} /> )} />}

                                            { isAdminUser && <Route path={`${process.env.PUBLIC_URL}/home/user/admin`} exact 
                                                render= { props => (<Admins setTitle={this.setTitle} searchText={this.state.searchText} {... props} /> )} /> }

                                            { isAdminUser && <Route path={`${process.env.PUBLIC_URL}/home/user/staff`} exact 
                                                render= { props => (<Agents setTitle={this.setTitle} searchText={this.state.searchText} {... props} /> )} /> }

                                            <Route path={`${process.env.PUBLIC_URL}/home/user/student`} exact 
                                                render= { props => (<Students setTitle={this.setTitle} searchText={this.state.searchText} 
                                                    navigateToApplication={this.navigateToApplication}{... props} /> )} />

                                            <Route path={`${process.env.PUBLIC_URL}/home/application`} exact 
                                                render= { props => (<ApplicantForm setTitle={this.setTitle} emailToNavigate={this.state.emailToNavigate}  {... props} /> )} />

                                            { isAdminUser && <Route path={`${process.env.PUBLIC_URL}/home/setting/staffMapping`} exact 
                                                render= { props => (<StaffMapping setTitle={this.setTitle} searchText={this.state.searchText} {... props} /> )} /> }

                                        </div>
                                    </Column>
                                </Row>
                            }
                        </Route>
                    </switch>
                </Router>
            </Fragment>          
        );
    }
}

export default Home;