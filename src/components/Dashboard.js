import React, {Component, Fragment} from 'react';
import { Chart } from "react-google-charts";
import { Draggable, Droppable } from "react-drag-and-drop";
import axios from 'axios';
import { Column, Row } from 'simple-flexbox';
import PipelineDashboard from './PipelineDashboard';

class Dashboard extends Component{
    constructor(props){
        super(props);

        this.state = {
            newStudents:[],
            pipelines: [],
            isStudent: localStorage.getItem('access_role') === 'STUDENT'
        }

        this.onDrop = this.onDrop.bind(this);
    }

    // Pipeline status :- ["OPENED","RE_OPENED","REGISTERED","VERIFIED","SUBMITTED_TO_UNIV","CONDITIONAL_OFFER_LETTER","CASA_VISA_RECVD","DEAL_CLOSED"]

    onDrop = (data) => {
        let email = ''
        let status = '';
        if(data.registered != null){
            if(data.registered.length === 0){
                status = 'REGISTERED';
            } else {
                email = data.registered;
            }            
        } 
        if(data.verified != null){
            if(data.verified.length === 0){
                status = 'VERIFIED';
            } else {
                email = data.verified;
            }            
        } 
        if(data.submitted_to_univ != null){
            if(data.submitted_to_univ.length === 0){
                status = 'SUBMITTED_TO_UNIV';
            } else {
                email = data.submitted_to_univ;
            }            
        } 
        if(data.conditional_offer_letter != null){
            if(data.conditional_offer_letter.length === 0){
                status = 'CONDITIONAL_OFFER_LETTER';
            } else {
                email = data.conditional_offer_letter;
            }            
        } 
        if(data.casa_visa_recvd != null){
            if(data.casa_visa_recvd.length === 0){
                status = 'CASA_VISA_RECVD';
            } else {
                email = data.casa_visa_recvd;
            }            
        } 
        if(data.deal_closed != null){
            if(data.deal_closed.length === 0){
                status = 'DEAL_CLOSED';
            } else {
                email = data.deal_closed;
            }            
        }

        if(status !== '' && email !== ''){
            const options = {
                headers: { 'access-token': localStorage.getItem('access_token')}
            };
            axios.get(process.env.REACT_APP_ENDPOINT + `applicant/pipeline/update/${email}/${status}`, options)
            .then(response => {
                if(response.status == 200){
                    this.setState({
                        pipelines: response.data
                    })
                }                
            }).catch(error => {
                console.log(error)
            });
        }        
    }

    componentDidMount() {
        const { setTitle } = this.props;
        setTitle('Dashboard');

        const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };

        axios.get(process.env.REACT_APP_ENDPOINT + `usermgm/user/all/STUDENT`, options)
            .then(res => {
                this.setState({ newStudents: res.data});
            }).catch(error => {
                console.log(error)
        });

        axios.get(process.env.REACT_APP_ENDPOINT + `applicant/pipelines`, options)
            .then(response => {
                if(response.status === 200){
                    this.setState({
                        pipelines: response.data
                    })
                } else if(response.status === 1015) {
                    // TODO
                }            
            })
            .catch(error => {
                console.log(error)
            });

    }


    render() { 
        const registeredObj = this.state.pipelines.map((data) => {
            if (data.status === "REGISTERED") {
              return (
                <Draggable type={data.status} data={data.emailAddress}>
                    <span>
                        @{data.applicantName} , {(new Date(data.submittedDate).toDateString())} , {data.mobileNumber} , {data.appliedFor} 
                        , <a style={{"backgroundColor":"yellow"}}>{data.verifiedPercentage}</a>
                    </span>
                </Draggable>
              );
            }
         });
        
        const verifiedObj = this.state.pipelines.map((data) => {
            if (data.status === "VERIFIED") {
                return (
                <Draggable type={data.status} data={data.emailAddress}>
                    <span>
                        @{data.applicantName} , {(new Date(data.submittedDate).toDateString())} , {data.mobileNumber} , {data.appliedFor} 
                        , <a style={{"backgroundColor":"yellow"}}>{data.verifiedPercentage}</a>
                    </span>
                </Draggable>
                );
            }
        });

        const submittedToUniObj = this.state.pipelines.map((data) => {
            if (data.status === "SUBMITTED_TO_UNIV") {
                return (
                <Draggable type={data.status} data={data.emailAddress}>
                    <span>
                        @{data.applicantName} , {(new Date(data.submittedDate).toDateString())} , {data.mobileNumber} , {data.appliedFor} 
                        , <a style={{"backgroundColor":"yellow"}}>{data.verifiedPercentage}</a>
                    </span>
                </Draggable>
                );
            }
        });

        const condOffLettObj = this.state.pipelines.map((data) => {
            if (data.status === "CONDITIONAL_OFFER_LETTER") {
                return (
                <Draggable type={data.status} data={data.emailAddress}>
                    <span>
                        @{data.applicantName} , {(new Date(data.submittedDate).toDateString())} , {data.mobileNumber} , {data.appliedFor} 
                        , <a style={{"backgroundColor":"yellow"}}>{data.verifiedPercentage}</a>
                    </span>
                </Draggable>
                );
            }
        });

        const casaReceivedObj = this.state.pipelines.map((data) => {
            if (data.status === "CASA_VISA_RECVD") {
                return (
                <Draggable type={data.status} data={data.emailAddress}>
                    <span>
                        @{data.applicantName} , {(new Date(data.submittedDate).toDateString())} , {data.mobileNumber} , {data.appliedFor} 
                        , <a style={{"backgroundColor":"yellow"}}>{data.verifiedPercentage}</a>
                    </span>
                </Draggable>
                );
            }
        });

        const dealClosedObj = this.state.pipelines.map((data) => {
            if (data.status === "DEAL_CLOSED") {
                return (
                <Draggable type={data.status} data={data.emailAddress}>
                    <span>
                        @{data.applicantName} , {(new Date(data.submittedDate).toDateString())} , {data.mobileNumber} , {data.appliedFor} 
                        , <a style={{"backgroundColor":"yellow"}}>{data.verifiedPercentage}</a>
                    </span>
                </Draggable>
                );
            }
        });
                                    
        return (
                <Fragment>
                    {!this.state.isStudent &&
                    <div>
                        <Row className="col-sm-12">
                            <Column className="card card-width-50 pre-scrollable card-margin-right">
                                <div className="row card-header card-hdr-height">
                                    <div className="col-sm-6">
                                        <h5>New Students</h5>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="pull-right right-header"><span>Month</span><span>
                                        <button className="btn btn-primary btn-pill">Today</button></span></div>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="new-users">
                                        {this.state.newStudents.map(student => (
                                            <div className="media">
                                                <img className="rounded-circle image-radius m-r-15" src={require('../assets/images/user/2.png')} alt="" />
                                                <div className="media-body">
                                                    <h6 className="mb-0 f-w-700">{student.firstName}</h6>
                                                    <p>Visual Designer, Github Inc</p>
                                                </div><span className="pull-right">
                                                    <button className="btn btn-pill btn-outline-light">View</button></span>
                                            </div>
                                          ))}
                                    </div>
                                </div>
                            </Column>
                            <Column className="card card-width-50">
                                <div className="row card-header card-hdr-height">
                                    <div className="col-xl-12">
                                        <h5>Activities</h5>
                                    </div>
                                </div>
                                <div className="card-body"> 
                                    <Chart
                                        width={'100%'}
                                        height={'400px'}
                                        chartType="PieChart"
                                        loader={<div>Loading Chart</div>}
                                        data={[
                                            ['Registered', 'Per Day'],
                                            ['Student', 40],
                                            ['Agents', 30],
                                            ['Admin', 30]
                                        ]}
                                        options={{
                                            title: 'Daily Registration',
                                            colors: ["#4466f2", "#1ea6ec", "#22af47", "#1b7aff", "#f85370"]
                                        }}
                                        rootProps={{ 'data-testid': '1' }}
                                    />
                                </div>
                            </Column>
                        </Row>
                        <PipelineDashboard />
                    </div>
                }                
            </Fragment>
        );
    }
}

export default Dashboard;