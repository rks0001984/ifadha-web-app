import React, { Component, Fragment } from 'react';
import axios from 'axios';
import { Typeahead } from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { Row, Button } from 'reactstrap';
import TableModel from './TableModel';

class StaffMapping extends Component{

    constructor(props){
        super(props);
        this.state = {
            projectNames: [],
            userEmails: [],
            selectedUsers: '',
            selectedProjects: [],
            projectUsers:[],
            columns: [
                { Header: "Sub Agency", accessor: "projectName" },
                { Header: "Staff Email", accessor: "userEmail" }
            ],
            selectedRows:[]
        }

        this.addSelectedRowObj = this.addSelectedRowObj.bind(this);
        this.removeUnSelectedRowObj = this.removeUnSelectedRowObj.bind(this);
    }

    addSelectedRowObj = selectedRows => {

        if (selectedRows.length != null && selectedRows.length != undefined && selectedRows.length > 0) {
            this.setState({ selectedRows: [...this.state.selectedRows, ...selectedRows] });
        } else {
            this.setState({ selectedRows: [...this.state.selectedRows, selectedRows] });
        }
    }

    removeUnSelectedRowObj = unSelectedRows => {
        if (unSelectedRows.length != null && unSelectedRows.length != undefined && unSelectedRows.length > 0) {
            const selectedObj = this.state.selectedRows.filter(row => !unSelectedRows.includes(row));
            this.setState({ selectedRows: selectedObj });
        } else {
            const selectedObj = this.state.selectedRows.filter(row => row !== unSelectedRows);
            this.setState({ selectedRows: selectedObj });
        }
    }

    componentDidMount() {
        const { setTitle } = this.props;
        setTitle('Mapping');

        const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };
        axios.get(process.env.REACT_APP_ENDPOINT + `usermgm/user/all/STAFF?sort=id,desc`, options)
        .then(response => {
            if(response.status === 200){
                response.data.map( user => {
                    this.setState({ 
                        userEmails: [...this.state.userEmails, user.emailAddress] 
                    })
                })
            } else if(response.status === 1015) {
                
            }
        }).catch(error => {
            console.log(error)
        });

        axios.get(process.env.REACT_APP_ENDPOINT+`admin/project/all?sort=id,desc`, options)
        .then(response => {
            if(response.status === 200) {
                response.data.map( project => {
                    this.setState({ 
                        projectNames: [...this.state.projectNames, project.name] 
                    })
                })
            }
        }).catch(error => {
            console.log(error);
        })

        axios.get(process.env.REACT_APP_ENDPOINT+`admin/project/user-group/access?sort=id,desc`, options)
        .then(res => {
             if(res.status === 200) {
                this.setState({
                    projectUsers: res.data,
                    selectedProjects: [],
                    selectedUsers: []
                })
            }
        }).catch(error => {
            console.log(error);
        })
    }

    addUserProjectMapping = () => {
        const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };
        axios.post(process.env.REACT_APP_ENDPOINT + 'admin/project/user-group/access/add', {
            projectNameList: this.state.selectedProjects,
            userEmailList: this.state.selectedUsers
        }, options).then(res => {
            if(res.status === 200) {
                this.setState({
                    projectUsers: res.data,
                    selectedProjects: [],
                    selectedUsers: []
                })
            }
        }).catch(error => {
           
        });
    }

    render() {
        return (
            <div>
                <Row className="col-xl-12">
                    <div className="col-xl-5 proj-map-drop-down" >
                        <p><b>Sub Agencies</b></p>
                        <Typeahead
                            id="multiple-typeahead"
                            clearButton
                            labelKey="name"
                            onChange={(selectedProjects) => {
                                this.setState({selectedProjects});
                            }}
                            multiple
                            options={this.state.projectNames}
                            placeholder="Select an Sub Agency..."
                        />
                    </div>
                    <div className="col-xl-5">
                        <p><b>Staffs</b></p>
                        <Typeahead
                            className="typea-head-dropdown"
                            id="multiple-typeahead"
                            clearButton
                            onChange={(selectedUsers) => {
                                this.setState({selectedUsers});
                            }}
                            labelKey="emailAddress"
                            multiple
                            options={this.state.userEmails}
                            placeholder="Select Staff to map..."
                        />
                    </div>
                    <div className="col-xl-2">
                       <div className="btn-center">
                            <Button variant="primary" disabled={this.state.selectedProjects.length === 0 || this.state.selectedUsers.length ===0}
                                onClick={() => this.addUserProjectMapping()}>Link 
                            </Button>
                        </div>
                    </div>
                </Row>
                <Row>
                    <TableModel data={this.state.projectUsers} columns={this.state.columns} addSelectedRowObj={this.addSelectedRowObj} 
                        removeUnSelectedRowObj={this.removeUnSelectedRowObj}/> 
                </Row>
            </div>
        );
    }
}

export default StaffMapping;