import React, { Component , Fragment } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import axios from 'axios';
import TableModel from './TableModel';
import SweetAlert from 'react-bootstrap-sweetalert';

import {PlusSquare, Trash, Edit} from 'react-feather';
import { StyleSheet, css } from 'aphrodite';

class Projects extends Component{
    constructor(props){
        super(props);
        this.state = {
            collapseAddProject: false,
            collapseEditProject: false,
            projectName: '',
            projectDesc: '',
            errors: {
                projectName: '', 
                projectDesc: ''
            },
            projects: [],
            columns: [
                { Header: "Name", accessor: "name" },
                { Header: "Description", accessor: "description" }
            ],
            selectedRows:[],
            searchText: this.props.searchText,
            showAlert: false,
            basicAlertTitle:'',
            basicAlertType:"default",
            showDelAlert: false
        }
        this.handleSubmit  = this.handleSubmit.bind(this);
        this.updateProject  = this.updateProject.bind(this);
        this.addSelectedRowObj = this.addSelectedRowObj.bind(this);
        this.removeUnSelectedRowObj = this.removeUnSelectedRowObj.bind(this);

        // preserve the initial state in a new object
        this.baseState = this.state 
    }

    resetForm = () => {
        this.setState(this.baseState)
    }

    toggleAddProjectBtn = () => {
        this.setState({
            projectName: '',
            projectDesc: '',
            collapseAddProject: !this.state.collapseAddProject
        })
    }

    toggleEditProjectBtn = () => {
        if(!this.state.collapseEditUser && this.state.selectedRows != null 
            && this.state.selectedRows != undefined && this.state.selectedRows.length === 1){
                this.setState({
                    projectName: this.state.selectedRows[0].name,
                    projectDesc: this.state.selectedRows[0].description
                })
        }
        this.setState({
            collapseEditProject: !this.state.collapseEditProject
        })
    }

    componentDidMount = () => {
        const { setTitle } = this.props;
        setTitle('Sub Agency');

        const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };
        axios.get(process.env.REACT_APP_ENDPOINT+`admin/project/all?sort=id,desc`, options)
            .then(response => {
                if(response.status === 200){
                    this.setState({
                        projects: response.data
                    })
                } else if(response.status === 1015) {
                    
                }
            }).catch(error => {
                this.setState({
                    errorMessage: "Unable to load projects"
                })
                console.log(error);
            })
    }

    handleSubmit = (event) => {
        event.preventDefault();
        if (this.state.errors.projectName.length > 0 || this.state.errors.projectDesc.length > 0 
            || this.state.projectName === "" || this.state.projectDesc === "") {
            return;
        } else {
            const options = {
                headers: { 'access-token': localStorage.getItem('access_token')}
            };
            axios.post(process.env.REACT_APP_ENDPOINT+`admin/project/create?sort=id,desc`, { 
                'name': this.state.projectName,
                'description': this.state.projectDesc            
            }, options).then(response => {
                if(response.status === 200) {
                    this.setState({
                        projects: response.data,
                        collapseAddProject:false,
                        showAlert: true,
                        basicAlertTitle: 'Project created successfully!',
                        basicAlertType: "success"
                    });
                }      
            }).catch(error => {
                this.setState({
                    showAlert: true,
                    basicAlertType: "error",
                    basicAlertTitle: 'Unable to create project'
                })
                console.log(error);
            })
            this.resetForm();
        }
    }

    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        let errors = this.state.errors;
    
        switch (name) {
            case 'projectName': 
                errors.projectName = 
                    value.length < 4
                    ? 'Name must be 4 characters long!'
                    : '';
                break;
            case 'projectDesc': 
                errors.projectDesc = 
                value.length < 20
                    ? 'Description must be 20 characters long!'
                    : '';
                break;
            default:
                break;
        }
        this.setState({errors, [name]: value});
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.searchText !== undefined && this.state.searchText !== nextProps.searchText ) {
            const { setTitle } = this.props;
            setTitle('Sub Agency');

            this.setState({
                searchText: nextProps.searchText
            })
            const filters = '';
    
            const options = {
                headers: { 'access-token': localStorage.getItem('access_token')}
            };
            axios.get(process.env.REACT_APP_ENDPOINT + `search/project?searchText=${nextProps.searchText}&filters=&sort=id,desc`, options)
            .then(res => {
                this.setState({ 
                    projects: res.data,
                    searchText: ''
                });
            }).catch(error => {
                this.setState({
                    errorMessage: 'Unable to search project'
                })
                console.log(error)
            });
        }
    }

    addSelectedRowObj = selectedRows => {

        if (selectedRows.length != null && selectedRows.length != undefined && selectedRows.length > 0) {
            this.setState({ selectedRows: [...this.state.selectedRows, ...selectedRows] });
        } else {
            this.setState({ selectedRows: [...this.state.selectedRows, selectedRows] });
        }
    }

    removeUnSelectedRowObj = unSelectedRows => {
        if (unSelectedRows.length != null && unSelectedRows.length != undefined && unSelectedRows.length > 0) {
            const selectedObj = this.state.selectedRows.filter(row => !unSelectedRows.includes(row));
            this.setState({ selectedRows: selectedObj });
        } else {
            const selectedObj = this.state.selectedRows.filter(row => row !== unSelectedRows);
            this.setState({ selectedRows: selectedObj });
        }
    }

    deleteProjects = () => {
        this.state.selectedRows.map( project => this.deleteProject(project) );
    }

    deleteProject = project => {
        if(project != null && project != undefined){
            const options = {
                headers: { 'access-token': localStorage.getItem('access_token')}
            };
            axios.delete(process.env.REACT_APP_ENDPOINT+`admin/project/delete/`+ project.id, options)
            .then(res => {
                if(res.status === 200){
                    this.removeUnSelectedRowObj(project);
                    this.reloadData();
                    this.setState({
                        showDelAlert: false
                    })
                }
            }).catch(error => {
                this.setState({
                    errorMessage: 'unable to delete project : '+project.projectName
                })
                console.log(error);
            });
        }        
    }

    reloadData = () => {
        const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };
        axios.get(process.env.REACT_APP_ENDPOINT+`admin/project/all?sort=id,desc`, options)
        .then(response => {               
            this.setState({                   
                projects: response.data
            })
        }).catch(error => {
            this.setState({
                errorMessage: "Unable to load projects"
            })
            console.log(error);
        })    
    }

    updateProject = (event) => {
        event.preventDefault();
        if (this.state.errors.projectName.length > 0 || this.state.errors.projectDesc.length > 0) {
            return;
        } else {
            const options = {
                headers: { 'access-token': localStorage.getItem('access_token')}
            };
            axios.put(process.env.REACT_APP_ENDPOINT+`admin/project/update?sort=id,desc`, {
                'name': this.state.projectName,
                'description': this.state.projectDesc,
                'id': this.state.selectedRows[0].id            
            }, options).then(response => {
                if(response.status === 200) {
                    this.setState({
                        projects: response.data,
                        collapseEditProject:false,
                        showAlert: true,
                        basicAlertTitle: 'Project Updated successfully!',
                        basicAlertType: "success"
                    });
                }      
            }).catch(error => {
                this.setState({
                    showAlert: true,
                    basicAlertTitle: 'Unable to update project',
                    basicAlertType: "error"
                });
            })
        } 
    }

    closeAlert = () => {
        this.setState({
            showAlert: false,
            showDelAlert: false
        });
    }

    showDelAlert = () => {
        this.setState({
            showDelAlert: true
        })
    }

    render() {
        const deleteBtn = <Button className="ml-2" color="button-color" onClick={this.showDelAlert} style={{ marginBottom: '1rem' }}>
        <Trash className="menuIcon"/></Button>
        const editBtn = <Fragment>
                            <Button className="ml-2" color="button-color" onClick={() => this.toggleEditProjectBtn()} style={{ marginBottom: '1rem' }}><Edit className="menuIcon"/></Button>
                            <Modal isOpen={this.state.collapseEditProject} toggle={() => this.toggleEditProjectBtn()}>
                                <ModalHeader toggle={() => this.toggleEditProjectBtn()}><Edit className="menuIcon"/> Sub Agency</ModalHeader>
                                <ModalBody>
                                <form className="theme-form" id="projectForm">
                                    <div className="form-row">
                                        <label className="col-form-label">Name</label>
                                        <input name="projectName" disabled={true} value={this.state.projectName} className="form-control" type="text" 
                                            placeholder="Sub Agency name" onChange={this.handleChange} />
                                    </div>
                                    <div className="form-row">
                                        <label className="col-form-label">Description</label>
                                        <input name="projectDesc" value={this.state.projectDesc} className="form-control" type="text"
                                            placeholder="Sub Agency description" onChange={this.handleChange}/>
                                    </div>
                                </form>
                                </ModalBody>
                                <ModalFooter>
                                    <Button className="btn-primary" onClick={() => this.toggleEditProjectBtn(), this.updateProject}>Save</Button>
                                    <Button color="secondary" onClick={() => this.toggleEditProjectBtn()}>Cancel</Button>
                                    <div className="form-group">
                                        <label className="srv-validation-message">{this.state.errors.projectName}</label><br/>
                                        <label className="srv-validation-message">{this.state.errors.projectDesc}</label>
                                    </div>
                                </ModalFooter>
                            </Modal>
                        </Fragment>
        const selectedRowCount = this.state.selectedRows.length;

        return (
            <div className="map-block">
                <div className="container-fluid-project ">
                    <SweetAlert
                        show={this.state.showAlert}
                        type={this.state.basicAlertType}
                        title={this.state.basicAlertTitle}
                        onConfirm={this.closeAlert}
                    />
                    <SweetAlert
                        show={this.state.showDelAlert}
                        warning
                        showCancel
                        confirmBtnText="Yes, delete it!"
                        confirmBtnBsStyle="danger"
                        title="Are you sure?"
                        onConfirm={this.deleteProjects}
                        onCancel={this.closeAlert}
                        focusCancelBtn
                        >
                        You will not be able to undo this delete!
                    </SweetAlert>
                    <div className="container-fluid-left">                       
                        <Button className="pl-4" color="button-color" onClick={() => this.toggleAddProjectBtn()} style={{ marginBottom: '1rem' }}>
                        <PlusSquare className="menuIcon"/> Sub Agency</Button>		
                        <Modal isOpen={this.state.collapseAddProject} toggle={() => this.toggleAddProjectBtn()}>
                            <ModalHeader toggle={() => this.toggleAddProjectBtn()}>New Sub Agency</ModalHeader>
                            <ModalBody>
                            <form className="theme-form" id="projectForm">
                                <div className="form-row">
                                    <label className="col-form-label">Name</label>
                                    <input name="projectName" value={this.state.projectName} required={true} className="form-control" type="text" 
                                        placeholder="Sub Agency name" onChange={this.handleChange} />
                                </div>
                                <div className="form-row">
                                    <label className="col-form-label">Description</label>
                                    <input name="projectDesc" value={this.state.projectDesc} className="form-control" type="text"
                                        placeholder="Sub Agency description" onChange={this.handleChange}/>
                                </div>
                            </form>
                            </ModalBody>
                            <ModalFooter>
                                <Button className="btn-primary" onClick={() => this.toggleAddProjectBtn(), this.handleSubmit}>Create</Button>
                                <Button color="secondary" onClick={() => this.toggleAddProjectBtn()}>Cancel</Button>
                                <div className="form-group">
                                    <label className="srv-validation-message">{this.state.errors.projectName}</label><br/>
                                    <label className="srv-validation-message">{this.state.errors.projectDesc}</label>
                                </div>
                            </ModalFooter>
                        </Modal>
                        {selectedRowCount > 0 && deleteBtn}
                        {selectedRowCount === 1 && editBtn}
                    </div>
                </div>                
                <TableModel data={this.state.projects} columns={this.state.columns} addSelectedRowObj={this.addSelectedRowObj} 
                    removeUnSelectedRowObj={this.removeUnSelectedRowObj}/>
            </div>
        );
    }
}

export default Projects;