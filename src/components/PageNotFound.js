import React, { Fragment } from 'react';
import sad from '../assets/images/other-images/sad.png';
import { Link } from 'react-router-dom';

const PageNotFound = () => {
  return (
    <Fragment>
      <div className="page-wrapper">
        <div className="error-wrapper">
          <div className="container"><img className="img-100" src={sad} alt="" />
              <h3 className="headline font-info">Unauthorise Access</h3>
            <div className="col-md-8 offset-md-2">
              <p className="sub-content">The page you are attempting to reach is currently not accesible or not found. This may be because you are redirected to wrong page.</p>
            </div>
            <div><Link to={`${process.env.PUBLIC_URL}/home/dashboard`} className="btn btn-info-gradien"> BACK TO DASHBOARD PAGE</Link></div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default PageNotFound;