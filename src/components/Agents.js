import React, { Component, Fragment} from 'react';
import axios from 'axios';
import TableModel from './TableModel';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import SweetAlert from 'react-bootstrap-sweetalert';

import {PlusSquare, Trash, Edit} from 'react-feather';
import { StyleSheet, css } from 'aphrodite';

const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);

class Agents extends Component{
    
    constructor(props){
        super(props);      

        this.state = ({
            collapseAddUser: false,
            collapseEditUser: false,
            firstName: '',
            lastName: '',
            emailAddress: '',
            password: '',
            mobileNumber: '',
            middleName:'',
            role: '',
            userType: '',
            username: '',
            orgName: '',
            filters:'',
            profileImageUri:'',
            projectIds:'',
            errors: {
                firstName: '', lastName: '', emailAddress: '', mobileNumber: '', username: ''
            },
            agents: [],
            users: [],
            projects: [],
            selectedProject: '',
            columns: [
                { Header: "First Name", accessor: "firstName" },
                { Header: "Last Name", accessor: "lastName" },
                { Header: "Mobile Number", accessor: "mobileNumber" },
                { Header: "Email", accessor: "emailAddress" },
                { Header: "Created By", accessor: "createdBy" }
            ],
            selectedRows:[],
            showAlert: false,
            basicAlertTitle:'',
            basicAlertType:"default",
            searchText: this.props.searchText,            
            navigateToApplication: this.props.navigateToApplication,
            showDelAlert: false
        });

        this.handleSubmit  = this.handleSubmit.bind(this);
        this.updateUser  = this.updateUser.bind(this);
        this.addSelectedRowObj = this.addSelectedRowObj.bind(this);
        this.removeUnSelectedRowObj = this.removeUnSelectedRowObj.bind(this);

        // preserve the initial state in a new object
        this.baseState = this.state 
    }

    resetForm = () => {
        this.setState(this.baseState)
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.searchText !== undefined && this.state.searchText !== nextProps.searchText) {
            const { setTitle } = this.props;
            setTitle('Staff');

            this.setState({
                searchText: nextProps.searchText
            })
    
            const options = {
                headers: { 'access-token': localStorage.getItem('access_token')}
            };
            axios.get(process.env.REACT_APP_ENDPOINT + `search/user?role=STAFF&searchText=${nextProps.searchText}&filters=&sort=id,desc`, options)
            .then(res => {
                this.setState({ users: res.data, searchText:''});
            }).catch(error => {
                console.log(error)
            });
        }        
    }

    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        let errors = this.state.errors;
    
        switch (name) {
            case 'firstName': 
                errors.firstName = 
                    value.length < 4
                    ? 'First Name must be 4 characters long!'
                    : '';
                break;
            case 'emailAddress': 
                errors.emailAddress = 
                validEmailRegex.test(value)
                    ? ''
                    : 'Provided Email Address is not valid!';
                break;
            case 'mobileNumber': 
                errors.mobileNumber = 
                value.length !== 10
                    ? 'Mobile number must be 10 characters long!'
                    : '';
                break;
            default:
                break;
        }
        this.setState({errors, [name]: value});
    }

    addSelectedRowObj = selectedRows => {
        if (selectedRows.length != null && selectedRows.length != undefined && selectedRows.length > 0) {
            this.setState({ selectedRows: [...this.state.selectedRows, ...selectedRows] });
        } else {
            this.setState({ selectedRows: [...this.state.selectedRows, selectedRows] });
        }
    }

    removeUnSelectedRowObj = unSelectedRows => {
        if (unSelectedRows.length != null && unSelectedRows.length != undefined && unSelectedRows.length > 0) {
            const selectedObj = this.state.selectedRows.filter(row => !unSelectedRows.includes(row));
            this.setState({ selectedRows: selectedObj });
        } else {
            const selectedObj = this.state.selectedRows.filter(row => row !== unSelectedRows);
            this.setState({ selectedRows: selectedObj });
        }
    }

    handleSubmit = event => {
        event.preventDefault();
        if (this.state.errors.firstName.length > 0 || this.state.errors.lastName.length > 0
                || this.state.errors.emailAddress.length > 0 || this.state.errors.mobileNumber.length > 0) {
            return;
        } else {
            const options = {
                headers: { 'access-token': localStorage.getItem('access_token')}
            };
            axios.post(process.env.REACT_APP_ENDPOINT + `usermgm/user/create?sort=id,desc`, {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                mobileNumber: this.state.mobileNumber,
                password: this.state.password,
                emailAddress: this.state.emailAddress,
                middleName: this.state.middleName,
                role: 'STAFF',
                orgName: this.state.orgName,
                projectNameList: this.state.selectedProject.split()
            }, options).then(res => {
                if(res.status === 200) {
                    this.setState({ 
                        users: res.data, 
                        collapseAddUser:false,
                        showAlert: true,
                        basicAlertType: "success",
                        basicAlertTitle: ' Staff ' + this.state.firstName + ' ' + this.state.lastName + ' Created!'
                    });
                    
                }
            }).catch(error => {
                this.setState({
                    showAlert: true,
                    basicAlertTitle: ' Staff failed to create!',
                    basicAlertType: "error"
                });
                this.resetForm();
            });
        }
    }

    updateUser = event => {
        event.preventDefault();
        if (this.state.errors.firstName.length > 0 || this.state.errors.lastName.length > 0
                || this.state.errors.emailAddress.length > 0 || this.state.errors.mobileNumber.length > 0) {
            return;
        } else {
            const options = {
                headers: { 'access-token': localStorage.getItem('access_token')}
            };
            axios.put(process.env.REACT_APP_ENDPOINT + `usermgm/user/update?sort=id,desc`, {
                id: this.state.selectedRows[0].id,
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                mobileNumber: this.state.mobileNumber,
                profileImageUri:this.state.profileImageUri,
                projectIds:this.state.projectIds

            }, options).then(res => {
                if(res.status === 200) {
                    this.setState({
                        users: res.data, 
                        collapseEditUser:false,
                        showAlert: true,
                        basicAlertTitle: ' Staff ' + this.state.firstName + ' ' + this.state.lastName + ' Updated!',
                        basicAlertType: "success"
                   });
                }
            }).catch(error => {
                this.setState({
                    basicAlertTitle: ' Staff failed to update!',
                    basicAlertType: "error",
                    showAlert: true
                });
            });
        }   

    }

    reloadData = () => {
        const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };
        axios.get(process.env.REACT_APP_ENDPOINT + `usermgm/user/all/STAFF?sort=id,desc`, options)
        .then(res => {
            this.setState({ 
                users: res.data,
                selectedRows: []
            });
        }).catch(error => {
            console.log(error)
        });     
    }

    componentDidMount() {
        const { setTitle } = this.props;
        setTitle('Staff');

        const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };
        axios.get(process.env.REACT_APP_ENDPOINT + `usermgm/user/all/STAFF?sort=id,desc`, options)
        .then(response => {
            if(response.status === 200){
                this.setState({ 
                    users: response.data
                });
            } else if(response.status === 1015) {
                
            }
        }).catch(error => {
            console.log(error)
        });

        axios.get(process.env.REACT_APP_ENDPOINT+`admin/project/all?sort=id,desc`, options)
        .then(response => {
            if(response.status === 200){
                this.setState({
                    projects: response.data
                })
            } else if(response.status === 1015) {
                
            }
        }).catch(error => {
            this.setState({
                errorMessage: "Unable to load projects"
            })
            console.log(error);
        })
    }

    toggleAddUserBtn = () => {
        this.setState({
            collapseAddUser: !this.state.collapseAddUser
        })
    }

    toggleEditUserBtn = () => {
        if(!this.state.collapseEditUser && this.state.selectedRows != null 
            && this.state.selectedRows != undefined && this.state.selectedRows.length === 1){
                this.setState({
                    firstName: this.state.selectedRows[0].firstName,
                    lastName: this.state.selectedRows[0].lastName,
                    mobileNumber: this.state.selectedRows[0].mobileNumber,
                    emailAddress: this.state.selectedRows[0].emailAddress
                })

        }
        this.setState({
            collapseEditUser: !this.state.collapseEditUser
        })
    }

    deleteUsers = () => {
        this.state.selectedRows.map( user => this.deleteUser(user) );
    }

    deleteUser = user => {
        if(user != null && user != undefined){
            const options = {
                headers: { 'access-token': localStorage.getItem('access_token')}
            };
            axios.delete(process.env.REACT_APP_ENDPOINT +`/usermgm/user/delete/`+ user.id, options)
            .then(res => {
                if(res.status === 200){
                    this.removeUnSelectedRowObj(user);
                    this.reloadData();
                    this.setState({
                        showDelAlert: false
                    })
                }
            }).catch(error => {
                this.setState({
                    errorMessage: 'unable to delete user : '+user.emailAddress
                })
                console.log(error);
            });
        }        
    }

    closeAlert = () => {
        this.setState({
            showAlert: false,
            showDelAlert: false
        });
    }

    showDelAlert = () => {
        this.setState({
            showDelAlert: true
        })
    }

    render() {
        const deleteBtn = <Button className="ml-2" color="button-color" onClick={this.showDelAlert} style={{ marginBottom: '1rem' }}><Trash className="menuIcon"/></Button>
        const editBtn = <Fragment>
                            <Button className="ml-2" color="button-color" onClick={() => this.toggleEditUserBtn()} style={{ marginBottom: '1rem' }}>
                            <Edit className="menuIcon"/></Button>
                            <Modal isOpen={this.state.collapseEditUser} toggle={() => this.toggleEditUserBtn()}>
                                <ModalHeader toggle={() => this.toggleEditUserBtn()}><Edit className="menuIcon"/> Staff</ModalHeader>
                                <ModalBody>
                                <form className="theme-form" id="signUpForm">
                                    <div className="form-row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label className="col-form-label">First Name</label>
                                                <input name="firstName" value={this.state.firstName} className="form-control" type="text" 
                                                    placeholder="Your First Name" onChange={this.handleChange} />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label className="col-form-label">Last Name</label>
                                                <input name="lastName" value={this.state.lastName} className="form-control" type="text"
                                                    placeholder="Your Last Name" onChange={this.handleChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label className="col-form-label">Email</label>
                                        <input name="emailAddress" className="form-control" type="text" disabled={true}  value={this.state.emailAddress}
                                            placeholder="Your Email Address" onChange={this.handleChange} />
                                    </div>
                                    <div className="form-row">                                 
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label className="col-form-label">Mobile Number</label>
                                                <input name="mobileNumber" className="form-control" type="text"  value={this.state.mobileNumber}
                                                    placeholder="Your Mobile Number" onChange={this.handleChange} />
                                            </div>
                                        </div>                                
                                    </div>
                                </form>
                                </ModalBody>
                                <ModalFooter>
                                    <Button className="btn-primary" onClick={() => this.toggleEditUserBtn(), this.updateUser}>Save</Button>
                                    <Button color="secondary" onClick={() => this.toggleEditUserBtn()}>Cancel</Button>
                                    <div className="form-group">
                                        <label className="srv-validation-message">{this.state.errors.firstName}</label><br/>
                                        <label className="srv-validation-message">{this.state.errors.lastName}</label>
                                        <label className="srv-validation-message">{this.state.errors.mobileNumber}</label>
                                    </div>
                                </ModalFooter>
                            </Modal>
                        </Fragment>
        const selectedRowCount = this.state.selectedRows.length;
        const projectOptions = this.state.projects.map((project) => <option key={project.name}>{project.name}</option>);
        return (            
            <div className="map-block">                
                <div className="container-fluid-project ">
                    <SweetAlert
                        show={this.state.showAlert}
                        type={this.state.basicAlertType}
                        title={this.state.basicAlertTitle}
                        onConfirm={this.closeAlert}
                    />
                    <SweetAlert
                        show={this.state.showDelAlert}
                        warning
                        showCancel
                        confirmBtnText="Yes, delete it!"
                        confirmBtnBsStyle="danger"
                        title="Are you sure?"
                        onConfirm={this.deleteUsers}
                        onCancel={this.closeAlert}
                        focusCancelBtn
                        >
                        You will not be able to undo this delete!
                    </SweetAlert>
                    <div className="container-fluid-left">          
                        <Button className="pl-4" color="button-color" onClick={() => this.toggleAddUserBtn()} style={{ marginBottom: '1rem' }}>
                        <PlusSquare className="menuIcon"/> Staff</Button>
                        <Modal isOpen={this.state.collapseAddUser} toggle={() => this.toggleAddUserBtn()}>
                            <ModalHeader toggle={() => this.toggleAddUserBtn()}>New Staff</ModalHeader>
                            <ModalBody>
                            <form className="theme-form" id="signUpForm">
                                <div className="form-row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="col-form-label">First Name</label>
                                            <input name="firstName" value={this.state.firstName} className="form-control" type="text" 
                                                placeholder="Your First Name" onChange={this.handleChange} />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="col-form-label">Last Name</label>
                                            <input name="lastName" value={this.state.lastName} className="form-control" type="text"
                                                placeholder="Your Last Name" onChange={this.handleChange}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="col-form-label">Email</label>
                                    <input name="emailAddress" className="form-control" type="text"  value={this.state.emailAddress}
                                        placeholder="Your Email Address" onChange={this.handleChange} />
                                </div>
                                <div className="form-row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="col-form-label">Mobile Number</label>
                                            <input name="mobileNumber" className="form-control" type="text"  value={this.state.mobileNumber}
                                                placeholder="Your Mobile Number" onChange={this.handleChange} />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="col-form-label">Sub Agency</label>
                                            <select className="form-control" name="selectedProject" 
                                                value={this.state.selectedProject} onChange={this.handleChange} >                                                
                                                {projectOptions}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-row">
                                    <label className="col-form-label">Password will be sent to provided email address.</label>
                                </div>
                            </form>
                            </ModalBody>
                            <ModalFooter>
                                <Button className="btn-primary" onClick={() => this.toggleAddUserBtn(), this.handleSubmit}>Add</Button>
                                <Button color="secondary" onClick={() => this.toggleAddUserBtn()}>Cancel</Button>
                                <div className="form-group">
                                    <label className="srv-validation-message">{this.state.errors.firstName}</label><br/>
                                    <label className="srv-validation-message">{this.state.errors.lastName}</label>
                                    <label className="srv-validation-message">{this.state.errors.emailAddress}</label>
                                    <label className="srv-validation-message">{this.state.errors.mobileNumber}</label>
                                </div>
                            </ModalFooter>
                        </Modal>
                        {selectedRowCount > 0 && deleteBtn}
                        {selectedRowCount === 1 && editBtn}
                    </div>
                </div>
                
                <TableModel data={this.state.users} columns={this.state.columns} addSelectedRowObj={this.addSelectedRowObj} 
                    removeUnSelectedRowObj={this.removeUnSelectedRowObj}/>                
                
            </div>
        );
    }
}

export default Agents;