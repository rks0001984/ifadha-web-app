import React, { Component, Fragment } from 'react';
import { User, LogOut, Maximize } from 'react-feather';
import userImage from '../assets/images/user/user.png';
import axios from 'axios';
import createBrowserHistory from 'history/createBrowserHistory';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import SweetAlert from 'react-bootstrap-sweetalert';


class UserMenu extends Component{
    constructor(props){
        super(props);
        this.state = {            
            profileImageUri: (localStorage.getItem('profileImageUri') !== "null" && localStorage.getItem('profileImageUri') !== "" ) ?  localStorage.getItem('profileImageUri') : userImage,
            history : createBrowserHistory(),
            dateOfBirth: '',
            emailAddress: localStorage.getItem('access_id'),
            enabled: '',
            firstName: '',
            middleName: '',
            lastName: '',
            fullName: '',
            mobileNumber: '',
            username: '',
            orgName: '',
            collapseEditProfile: false,
            education: '',
            country:'',
            addressOne:'',
            addressTwo: '',
            gender:'',
            showAlert: false,
            basicAlertTitle:'',
            basicAlertType:"default",
            errors: {
                firstName: '', lastName: ''
            },
        }

        this.updateProfile  = this.updateProfile.bind(this);
    }

    closeAlert = () => {
        this.setState({
            showAlert: false
        });
    }

    logOut = () => {
        const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };
        axios.get(process.env.REACT_APP_ENDPOINT + `index/logout`, options)
            .catch(error => {
                console.log(error)
                
            });
        localStorage.removeItem('access_token');
        localStorage.removeItem('access_id');
        localStorage.removeItem('access_role');
        localStorage.removeItem('access_fullname');
        localStorage.removeItem('profileImageUri');
        localStorage.clear();
        this.state.history.push("/");
    }

    loadProfile = () =>{
        const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };
        axios.get(process.env.REACT_APP_ENDPOINT + `usermgm/user/${this.state.emailAddress}`, options)
            .then(response => {
                if(response.status == 200){
                    const data = response.data;
                    this.setState({
                        firstName: data.firstName,
                        middleName: data.middleName,
                        lastName: data.lastName,
                        mobileNumber: data.mobileNumber,
                        profileImageUri: data.profileImageUri,
                        gender: data.gender,
                        education: data.userProfile != null ? data.userProfile.education : '',
                        country: data.userProfile != null && data.userProfile.country != null ? data.userProfile.country.nicename : '',
                        addressOne: data.userProfile != null ? data.userProfile.addressOne : '',
                        addressTwo: data.userProfile != null ? data.userProfile.addressTwo : '',
                        collapseEditProfile: !this.state.collapseEditProfile
                    })  
                }
                
            })
            .catch(error => {
                console.log(error)
                
            });
    }

    updateProfile = event => {
        event.preventDefault();
        if (this.state.errors.firstName.length > 0 || this.state.errors.lastName.length > 0) {
            return;
        } else {
            const options = {
                headers: { 'access-token': localStorage.getItem('access_token')}
            };
            axios.put(process.env.REACT_APP_ENDPOINT + `usermgm/curr/user/update`, {
                firstName: this.state.firstName,
                middleName: this.state.middleName,
                lastName: this.state.lastName,
                mobileNumber: this.state.mobileNumber,
                gender: this.state.gender,
                education: this.state.education,
                country: this.state.country,
                addressOne: this.state.addressOne,
                addressTwo: this.state.addressTwo
            }, options).then(res => {
                if(res.status === 200) {
                    this.setState({
                        collapseEditProfile:false,
                        showAlert: true,
                        basicAlertTitle: ' User profile ' + this.state.firstName + ' ' + this.state.lastName + ' Updated!',
                        basicAlertType: "success"
                   });
                }
            }).catch(error => {
                this.setState({
                    basicAlertTitle: ' User profile failed to update!',
                    basicAlertType: "error",
                    showAlert: true
                });
            });
        }
    }

    toggleEditProfile = () => {
        this.setState({
            collapseEditProfile: !this.state.collapseEditProfile
        })
    }

    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        let errors = this.state.errors;    
        switch (name) {
            case 'firstName': 
                errors.firstName = 
                    value.length < 4
                    ? 'First Name must be 4 characters long!'
                    : '';
                break;
            default:
                break;
        }
        this.setState({errors, [name]: value});
    }

    //full screen function
    goFull = () => {
        if ((document.fullScreenElement && document.fullScreenElement !== null) ||
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    }

    uploadProfileImage = e => {
        const [file] = e.target.files;
        const formData = new FormData(); 
        // Update the formData object 
        formData.append( 
            "file", 
            file, 
            file.name 
        ); 
     
        const options = {
            headers: { 'access-token': localStorage.getItem('access_token'), 'Content-Type': 'multipart/form-data'}
        };
        axios.post(process.env.REACT_APP_ENDPOINT + 'usermgm/curr/user/profile/img/upload', formData,options).then(res => {
            if(res.status === 200) {
                this.setState({
                    profileImageUri:res.data
                });
                localStorage.setItem('profileImageUri', res.data);
            }
        }).catch(error => {
            this.setState({
                showAlert: true,
                basicAlertTitle: 'Unbale to upload image!',
                basicAlertType: "error"
            })
        });
    }

    render() {
        const userImg = (this.state.profileImageUri !== "" && this.state.profileImageUri !== null) ?  this.state.profileImageUri : userImage;
        const editProfile = <Fragment>
                                <Modal size="lg" className="profile-modal" isOpen={this.state.collapseEditProfile} toggle={() => this.toggleEditProfile()} >
                                    <ModalHeader toggle={() => this.toggleEditProfile()}>
                                        <div className="media align-items-center">
                                            <img name="userImage" className="align-self-center pull-right img-100 img-h100 rounded-circle blur-up lazyloaded" 
                                                src={userImg} />
                                            <div className="inputWrapper">
                                                <input className="fileInput" type="file" accept="image/*" multiple = "false" 
                                                onChange={this.uploadProfileImage}/>
                                            </div>
                                        </div>
                                    </ModalHeader>
                                    <ModalBody>
                                        <form  className="theme-form" id="signUpForm">
                                            <div className="form-row">
                                                <div className="col-md-4">
                                                    <div className="form-group">
                                                        <label className="col-form-label">First Name</label>
                                                        <input name="firstName" required value={this.state.firstName} className="form-control" type="text" 
                                                            placeholder="Your First Name" onChange={this.handleChange} />
                                                    </div>
                                                </div>
                                                <div className="col-md-4">
                                                    <div className="form-group">
                                                        <label className="col-form-label">Middle Name</label>
                                                        <input name="middleName" value={this.state.middleName} className="form-control" type="text"
                                                            placeholder="Your Middle Name" onChange={this.handleChange}/>
                                                    </div>
                                                </div>
                                                <div className="col-md-4">
                                                    <div className="form-group">
                                                        <label className="col-form-label">Last Name</label>
                                                        <input name="lastName" required={true} value={this.state.lastName} className="form-control" type="text"
                                                            placeholder="Your Last Name" onChange={this.handleChange}/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-row">
                                                <div className="col-md-4">
                                                    <div className="form-group">
                                                        <label className="col-form-label">Gender</label>
                                                        <select className="form-control" name="gender" 
                                                            value={this.state.gender} onChange={this.handleChange} >
                                                            <option value="--Select">--Select--</option>
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                            <option value="Other">Other</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                 <div className="col-md-4">
                                                    <div className="form-group">
                                                        <label className="col-form-label">Mobile Number</label>
                                                        <input name="mobileNumber" required={true} className="form-control" type="text"  value={this.state.mobileNumber}
                                                            placeholder="Your Mobile Number" onChange={this.handleChange} />
                                                    </div>
                                                </div>
                                                <div className="col-md-4">
                                                    <div className="form-group">
                                                        <label className="col-form-label">Email</label>
                                                        <input name="emailAddress" className="form-control" type="text" disabled={true}  value={this.state.emailAddress}
                                                            placeholder="Your Email Address" onChange={this.handleChange} />
                                                    </div>
                                                </div>  
                                            </div>
                                            <div className="form-row">                                                
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <label className="col-form-label">Education</label>
                                                        <input name="education" className="form-control" type="text"  
                                                            value={this.state.education} onChange={this.handleChange} />
                                                    </div>
                                                </div>                            
                                            </div>
                                            <div className="form-row">
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <label className="col-form-label">Country</label>
                                                        <select className="form-control" name="country" 
                                                            value={this.state.country} onChange={this.handleChange} >
                                                            <option value="--Select Country--">--Select Country--</option>
                                                            <option value="Afganistan">Afghanistan</option>
                                                            <option value="Albania">Albania</option>
                                                            <option value="Algeria">Algeria</option>
                                                            <option value="American Samoa">American Samoa</option>
                                                            <option value="Andorra">Andorra</option>
                                                            <option value="Angola">Angola</option>
                                                            <option value="Anguilla">Anguilla</option>
                                                            <option value="Antigua & Barbuda">Antigua & Barbuda</option>
                                                            <option value="Argentina">Argentina</option>
                                                            <option value="Armenia">Armenia</option>
                                                            <option value="Aruba">Aruba</option>
                                                            <option value="Australia">Australia</option>
                                                            <option value="Austria">Austria</option>
                                                            <option value="Azerbaijan">Azerbaijan</option>
                                                            <option value="Bahamas">Bahamas</option>
                                                            <option value="Bahrain">Bahrain</option>
                                                            <option value="Bangladesh">Bangladesh</option>
                                                            <option value="Barbados">Barbados</option>
                                                            <option value="Belarus">Belarus</option>
                                                            <option value="Belgium">Belgium</option>
                                                            <option value="Belize">Belize</option>
                                                            <option value="Benin">Benin</option>
                                                            <option value="Bermuda">Bermuda</option>
                                                            <option value="Bhutan">Bhutan</option>
                                                            <option value="Bolivia">Bolivia</option>
                                                            <option value="Bonaire">Bonaire</option>
                                                            <option value="Bosnia & Herzegovina">Bosnia & Herzegovina</option>
                                                            <option value="Botswana">Botswana</option>
                                                            <option value="Brazil">Brazil</option>
                                                            <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                                                            <option value="Brunei">Brunei</option>
                                                            <option value="Bulgaria">Bulgaria</option>
                                                            <option value="Burkina Faso">Burkina Faso</option>
                                                            <option value="Burundi">Burundi</option>
                                                            <option value="Cambodia">Cambodia</option>
                                                            <option value="Cameroon">Cameroon</option>
                                                            <option value="Canada">Canada</option>
                                                            <option value="Canary Islands">Canary Islands</option>
                                                            <option value="Cape Verde">Cape Verde</option>
                                                            <option value="Cayman Islands">Cayman Islands</option>
                                                            <option value="Central African Republic">Central African Republic</option>
                                                            <option value="Chad">Chad</option>
                                                            <option value="Channel Islands">Channel Islands</option>
                                                            <option value="Chile">Chile</option>
                                                            <option value="China">China</option>
                                                            <option value="Christmas Island">Christmas Island</option>
                                                            <option value="Cocos Island">Cocos Island</option>
                                                            <option value="Colombia">Colombia</option>
                                                            <option value="Comoros">Comoros</option>
                                                            <option value="Congo">Congo</option>
                                                            <option value="Cook Islands">Cook Islands</option>
                                                            <option value="Costa Rica">Costa Rica</option>
                                                            <option value="Cote DIvoire">Cote DIvoire</option>
                                                            <option value="Croatia">Croatia</option>
                                                            <option value="Cuba">Cuba</option>
                                                            <option value="Curaco">Curacao</option>
                                                            <option value="Cyprus">Cyprus</option>
                                                            <option value="Czech Republic">Czech Republic</option>
                                                            <option value="Denmark">Denmark</option>
                                                            <option value="Djibouti">Djibouti</option>
                                                            <option value="Dominica">Dominica</option>
                                                            <option value="Dominican Republic">Dominican Republic</option>
                                                            <option value="East Timor">East Timor</option>
                                                            <option value="Ecuador">Ecuador</option>
                                                            <option value="Egypt">Egypt</option>
                                                            <option value="El Salvador">El Salvador</option>
                                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                            <option value="Eritrea">Eritrea</option>
                                                            <option value="Estonia">Estonia</option>
                                                            <option value="Ethiopia">Ethiopia</option>
                                                            <option value="Falkland Islands">Falkland Islands</option>
                                                            <option value="Faroe Islands">Faroe Islands</option>
                                                            <option value="Fiji">Fiji</option>
                                                            <option value="Finland">Finland</option>
                                                            <option value="France">France</option>
                                                            <option value="French Guiana">French Guiana</option>
                                                            <option value="French Polynesia">French Polynesia</option>
                                                            <option value="French Southern Ter">French Southern Ter</option>
                                                            <option value="Gabon">Gabon</option>
                                                            <option value="Gambia">Gambia</option>
                                                            <option value="Georgia">Georgia</option>
                                                            <option value="Germany">Germany</option>
                                                            <option value="Ghana">Ghana</option>
                                                            <option value="Gibraltar">Gibraltar</option>
                                                            <option value="Great Britain">Great Britain</option>
                                                            <option value="Greece">Greece</option>
                                                            <option value="Greenland">Greenland</option>
                                                            <option value="Grenada">Grenada</option>
                                                            <option value="Guadeloupe">Guadeloupe</option>
                                                            <option value="Guam">Guam</option>
                                                            <option value="Guatemala">Guatemala</option>
                                                            <option value="Guinea">Guinea</option>
                                                            <option value="Guyana">Guyana</option>
                                                            <option value="Haiti">Haiti</option>
                                                            <option value="Hawaii">Hawaii</option>
                                                            <option value="Honduras">Honduras</option>
                                                            <option value="Hong Kong">Hong Kong</option>
                                                            <option value="Hungary">Hungary</option>
                                                            <option value="Iceland">Iceland</option>
                                                            <option value="Indonesia">Indonesia</option>
                                                            <option value="India">India</option>
                                                            <option value="Iran">Iran</option>
                                                            <option value="Iraq">Iraq</option>
                                                            <option value="Ireland">Ireland</option>
                                                            <option value="Isle of Man">Isle of Man</option>
                                                            <option value="Israel">Israel</option>
                                                            <option value="Italy">Italy</option>
                                                            <option value="Jamaica">Jamaica</option>
                                                            <option value="Japan">Japan</option>
                                                            <option value="Jordan">Jordan</option>
                                                            <option value="Kazakhstan">Kazakhstan</option>
                                                            <option value="Kenya">Kenya</option>
                                                            <option value="Kiribati">Kiribati</option>
                                                            <option value="Korea North">Korea North</option>
                                                            <option value="Korea Sout">Korea South</option>
                                                            <option value="Kuwait">Kuwait</option>
                                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                            <option value="Laos">Laos</option>
                                                            <option value="Latvia">Latvia</option>
                                                            <option value="Lebanon">Lebanon</option>
                                                            <option value="Lesotho">Lesotho</option>
                                                            <option value="Liberia">Liberia</option>
                                                            <option value="Libya">Libya</option>
                                                            <option value="Liechtenstein">Liechtenstein</option>
                                                            <option value="Lithuania">Lithuania</option>
                                                            <option value="Luxembourg">Luxembourg</option>
                                                            <option value="Macau">Macau</option>
                                                            <option value="Macedonia">Macedonia</option>
                                                            <option value="Madagascar">Madagascar</option>
                                                            <option value="Malaysia">Malaysia</option>
                                                            <option value="Malawi">Malawi</option>
                                                            <option value="Maldives">Maldives</option>
                                                            <option value="Mali">Mali</option>
                                                            <option value="Malta">Malta</option>
                                                            <option value="Marshall Islands">Marshall Islands</option>
                                                            <option value="Martinique">Martinique</option>
                                                            <option value="Mauritania">Mauritania</option>
                                                            <option value="Mauritius">Mauritius</option>
                                                            <option value="Mayotte">Mayotte</option>
                                                            <option value="Mexico">Mexico</option>
                                                            <option value="Midway Islands">Midway Islands</option>
                                                            <option value="Moldova">Moldova</option>
                                                            <option value="Monaco">Monaco</option>
                                                            <option value="Mongolia">Mongolia</option>
                                                            <option value="Montserrat">Montserrat</option>
                                                            <option value="Morocco">Morocco</option>
                                                            <option value="Mozambique">Mozambique</option>
                                                            <option value="Myanmar">Myanmar</option>
                                                            <option value="Nambia">Nambia</option>
                                                            <option value="Nauru">Nauru</option>
                                                            <option value="Nepal">Nepal</option>
                                                            <option value="Netherland Antilles">Netherland Antilles</option>
                                                            <option value="Netherlands">Netherlands (Holland, Europe)</option>
                                                            <option value="Nevis">Nevis</option>
                                                            <option value="New Caledonia">New Caledonia</option>
                                                            <option value="New Zealand">New Zealand</option>
                                                            <option value="Nicaragua">Nicaragua</option>
                                                            <option value="Niger">Niger</option>
                                                            <option value="Nigeria">Nigeria</option>
                                                            <option value="Niue">Niue</option>
                                                            <option value="Norfolk Island">Norfolk Island</option>
                                                            <option value="Norway">Norway</option>
                                                            <option value="Oman">Oman</option>
                                                            <option value="Pakistan">Pakistan</option>
                                                            <option value="Palau Island">Palau Island</option>
                                                            <option value="Palestine">Palestine</option>
                                                            <option value="Panama">Panama</option>
                                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                                            <option value="Paraguay">Paraguay</option>
                                                            <option value="Peru">Peru</option>
                                                            <option value="Phillipines">Philippines</option>
                                                            <option value="Pitcairn Island">Pitcairn Island</option>
                                                            <option value="Poland">Poland</option>
                                                            <option value="Portugal">Portugal</option>
                                                            <option value="Puerto Rico">Puerto Rico</option>
                                                            <option value="Qatar">Qatar</option>
                                                            <option value="Republic of Montenegro">Republic of Montenegro</option>
                                                            <option value="Republic of Serbia">Republic of Serbia</option>
                                                            <option value="Reunion">Reunion</option>
                                                            <option value="Romania">Romania</option>
                                                            <option value="Russia">Russia</option>
                                                            <option value="Rwanda">Rwanda</option>
                                                            <option value="St Barthelemy">St Barthelemy</option>
                                                            <option value="St Eustatius">St Eustatius</option>
                                                            <option value="St Helena">St Helena</option>
                                                            <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                                                            <option value="St Lucia">St Lucia</option>
                                                            <option value="St Maarten">St Maarten</option>
                                                            <option value="St Pierre & Miquelon">St Pierre & Miquelon</option>
                                                            <option value="St Vincent & Grenadines">St Vincent & Grenadines</option>
                                                            <option value="Saipan">Saipan</option>
                                                            <option value="Samoa">Samoa</option>
                                                            <option value="Samoa American">Samoa American</option>
                                                            <option value="San Marino">San Marino</option>
                                                            <option value="Sao Tome & Principe">Sao Tome & Principe</option>
                                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                                            <option value="Senegal">Senegal</option>
                                                            <option value="Seychelles">Seychelles</option>
                                                            <option value="Sierra Leone">Sierra Leone</option>
                                                            <option value="Singapore">Singapore</option>
                                                            <option value="Slovakia">Slovakia</option>
                                                            <option value="Slovenia">Slovenia</option>
                                                            <option value="Solomon Islands">Solomon Islands</option>
                                                            <option value="Somalia">Somalia</option>
                                                            <option value="South Africa">South Africa</option>
                                                            <option value="Spain">Spain</option>
                                                            <option value="Sri Lanka">Sri Lanka</option>
                                                            <option value="Sudan">Sudan</option>
                                                            <option value="Suriname">Suriname</option>
                                                            <option value="Swaziland">Swaziland</option>
                                                            <option value="Sweden">Sweden</option>
                                                            <option value="Switzerland">Switzerland</option>
                                                            <option value="Syria">Syria</option>
                                                            <option value="Tahiti">Tahiti</option>
                                                            <option value="Taiwan">Taiwan</option>
                                                            <option value="Tajikistan">Tajikistan</option>
                                                            <option value="Tanzania">Tanzania</option>
                                                            <option value="Thailand">Thailand</option>
                                                            <option value="Togo">Togo</option>
                                                            <option value="Tokelau">Tokelau</option>
                                                            <option value="Tonga">Tonga</option>
                                                            <option value="Trinidad & Tobago">Trinidad & Tobago</option>
                                                            <option value="Tunisia">Tunisia</option>
                                                            <option value="Turkey">Turkey</option>
                                                            <option value="Turkmenistan">Turkmenistan</option>
                                                            <option value="Turks & Caicos Is">Turks & Caicos Is</option>
                                                            <option value="Tuvalu">Tuvalu</option>
                                                            <option value="Uganda">Uganda</option>
                                                            <option value="United Kingdom">United Kingdom</option>
                                                            <option value="Ukraine">Ukraine</option>
                                                            <option value="United Arab Erimates">United Arab Emirates</option>
                                                            <option value="United States of America">United States of America</option>
                                                            <option value="Uraguay">Uruguay</option>
                                                            <option value="Uzbekistan">Uzbekistan</option>
                                                            <option value="Vanuatu">Vanuatu</option>
                                                            <option value="Vatican City State">Vatican City State</option>
                                                            <option value="Venezuela">Venezuela</option>
                                                            <option value="Vietnam">Vietnam</option>
                                                            <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                                                            <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                                                            <option value="Wake Island">Wake Island</option>
                                                            <option value="Wallis & Futana Is">Wallis & Futana Is</option>
                                                            <option value="Yemen">Yemen</option>
                                                            <option value="Zaire">Zaire</option>
                                                            <option value="Zambia">Zambia</option>
                                                            <option value="Zimbabwe">Zimbabwe</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <label className="col-form-label">Address 1</label>
                                                        <input name="addressOne" className="form-control"
                                                            placeholder="Address Line 1" value={this.state.addressOne} onChange={this.handleChange} />
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="col-form-label">Address 2</label>
                                                        <input name="addressTwo" className="form-control"
                                                            placeholder="Address Line 2" value={this.state.addressTwo} onChange={this.handleChange} />
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </ModalBody>
                                    <ModalFooter>
                                        <div className="form-row">
                                            <label className="srv-validation-message">{this.state.errors.firstName}</label><br/>
                                            <label className="srv-validation-message">{this.state.errors.lastName}</label>
                                        </div>
                                        <div className="form-row">
                                            <Button className="btn-primary" onClick={() => this.toggleEditProfile(), this.updateProfile}>Update</Button>
                                            &nbsp;<Button color="secondary" onClick={() => this.toggleEditProfile()}>Cancel</Button>
                                        </div>
                                        
                                    </ModalFooter>
                                </Modal>
                            </Fragment>
        return (
            <Fragment>
                <SweetAlert
                    show={this.state.showAlert}
                    type={this.state.basicAlertType}
                    title={this.state.basicAlertTitle}
                    onConfirm={this.closeAlert}
                />
                <ul>
                    <li className="onhover-dropdown">
                        <div className="media align-items-center">
                            <img className="align-self-center pull-right img-50 img-h50 rounded-circle blur-up lazyloaded" src={userImg} />
                            <div className="dotted-animation">
                                <span className="animate-circle"></span>
                                <span className="main-circle"></span>
                            </div>
                            <div className="profile-dropdow-cont">
                                <ul className="profile-dropdown onhover-show-div p-20 profile-dropdown-hover">
                                    <li><User /><a onClick={this.loadProfile}><span>Edit Profile</span></a></li>
                                    <li><Maximize /><a onClick={this.goFull}><span>Full Screen</span></a></li>
                                    <li><LogOut /><a onClick={this.logOut} href="#!" ><span>Log out</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
                {editProfile}
            </Fragment>
        )
    }
}

export default UserMenu;