import React, { Component } from 'react';
import { Row, Column } from 'simple-flexbox';
import IconSearch from '../assets/icon-search';
import UserMenu from './UserMenu';
import { Maximize } from 'react-feather';

class HeaderComponent extends Component {
    constructor(props){
        super(props);
        this.state = {
            title: '',
            searchText: '',
            searchTextLocally: ''
        }
    }

    componentWillReceiveProps = (nextProps) => {
        if(nextProps.title !== this.state.title) {
            this.setState({
                title: nextProps.title,
                searchTextLocally: ''
            })
        }
    }

    onSearch = () => {
        const { setSearchText } = this.props;
        setSearchText(this.state.searchTextLocally);
    }

    setSearchTextLocally = event => {
        this.setState({
            searchTextLocally: event.target.value
        })
    }

    render() {
        return (
            <Row>
                <Column className="col-6" alignItems="start"><h2>{this.state.title}</h2></Column>
                <Column className="col-4" alignItems="center">
                    <form className="form-inline search-form">
                        <div >
                            <span className="float-left">
                                <input
                                    className="form-control-plaintext searchIcon"
                                    type="text"
                                    name="searchField"
                                    placeholder="Enter text here to search ..."
                                    defaultValue=""  
                                    onChange = { this.setSearchTextLocally }
                                    onKeyPress={event => {
                                        if (event.key === "Enter") {
                                            event.preventDefault();
                                            this.onSearch();
                                        }
                                    }}                       
                                />
                            </span>
                            <span className="float-right search-icon" onClick={this.onSearch}><IconSearch /></span>
                        </div> 
                    </form>
                </Column>
                <Column>
                    <span className="logged-user-name text-dark m-t-15">{localStorage.getItem('access_fullname')}</span>
                </Column>
                <Column>
                    <div>
                        <UserMenu/>
                    </div>
                </Column>
            </Row>
        )
    }; 
}

export default HeaderComponent;
