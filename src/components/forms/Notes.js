import React, {Component, Fragment} from 'react';
import { Button } from 'reactstrap';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

class Notes extends Component {

    constructor(){
        super();
        this.state = {
             activeTab: '1'          
        }
    }

    setActiveTab = (activeTabName) =>{
        this.setState({
            activeTab: activeTabName
        })
    }

    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        this.setState({[name]: value});
    }

    handleSave = () => {
        //console.log(this.state)
    }

    render(){
        return (
            <Fragment>
                <form className="theme-form theme-from-scroll" id="notesForm" >
                    <div className="card">
                        <div className="card-body">
                            <div className="form-row">
                                <div className="col-sm-12">
                                    <Nav tabs className="border-tab-primary">
                                        <NavItem className="nav nav-tabs" id="myTab" role="tablist">
                                            <NavLink className={this.state.activeTab === '1' ? 'active' : ''} onClick={() => this.setActiveTab('1')}>
                                                Note
                                            </NavLink>
                                        </NavItem>
                                        <NavItem className="nav nav-tabs" id="myTab" role="tablist">
                                            <NavLink className={this.state.activeTab === '2' ? 'active' : ''} onClick={() => this.setActiveTab('2')}>
                                                History
                                            </NavLink>
                                        </NavItem>
                                    </Nav>
                                    <TabContent activeTab={this.state.activeTab}>
                                        <TabPane tabId="1">
                                            <div className="form-row">
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <input name="notes" className="form-control my-4"
                                                            placeholder="Notes" onChange={this.handleChange} />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-row">
                                                <div className="col-md-4">
                                                    <div className="form-group">
                                                        <label className="col-form-label">Date</label>
                                                        <DatePicker name="dateAssigned" className="form-control digits" 
                                                            selected={new Date()} onChange={this.handleChange} />
                                                    </div>
                                                </div>
                                            </div>
                                        </TabPane>
                                        <TabPane tabId="2">
                                            <p className="mb-0 m-t-30">Upcoming History Page</p>
                                        </TabPane>
                                    </TabContent>
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="col-md-1 mr-3">
                                    <Button variant="primary" onClick={this.handleSave}>Save </Button>
                                </div>                              
                            </div>
                        </div>
                    </div>
                </form>
            </Fragment>
        )
    }
}

export default Notes;