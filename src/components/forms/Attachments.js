import React, {Component, Fragment} from 'react';
import { Button } from 'reactstrap';
import Dropzone from 'react-dropzone-uploader';
import 'react-dropzone-uploader/dist/styles.css';
import axios from 'axios';
import SweetAlert from 'react-bootstrap-sweetalert';
import TableModel from '../TableModel';

import {Download} from 'react-feather';
import { StyleSheet, css } from 'aphrodite';

class Attachments extends Component {
     constructor(props){
        super(props);
        this.state = {
            activeTab: '1' ,
            attachments:[],
            fileName: '',
            description: '',
            regEmailAddress: '',
            showAlert: false,
            basicAlertTitle:'',
            basicAlertType:"default",
            columns: [
                { Header: "File Name", accessor: "fileName" },
                { Header: "Description", accessor: "description" }
            ]
        }
        
        this.addSelectedRowObj = this.addSelectedRowObj.bind(this);
        this.removeUnSelectedRowObj = this.removeUnSelectedRowObj.bind(this);
    }

    addSelectedRowObj = selectedRows => {
        // Do nothing
    }

    removeUnSelectedRowObj = unSelectedRows => {
       // Do nothing
    }

    setActiveTab = (activeTabName) =>{
        this.setState({
            activeTab: activeTabName
        })
    }

    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        this.state.description = value;
    }

    handleSave = () => {
        const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };
        axios.post(process.env.REACT_APP_ENDPOINT + 'applicant/attachment/create-update', {
                "fileName": this.state.fileName,
                "description": this.state.description,
                "regEmailAddress": this.state.regEmailAddress
            }, options).then(res => {
            if(res.status === 200) {
                this.setState({
                    attachments: res.data,
                    showAlert: true,
                    basicAlertTitle: 'File uploaded',
                    basicAlertType: "success",
                    fileName: "",
                    description: ""
                })
            }
        }).catch(error => {
            this.setState({
                showAlert: true,
                basicAlertTitle: 'Unable to upload!',
                basicAlertType: "error"
            })
        });
    }

    // receives array of files that are done uploading when submit button is clicked
    handleSubmit = (files, allFiles) => {
        allFiles.forEach(f => f.remove());
    }

    // specify upload params and url for your files
    getUploadParams = ({ file }) => {
        
        const formData = new FormData(); 
        formData.append( 
             "file", 
             file, 
             file.name 
         );
        const hdr = { 'access-token': localStorage.getItem('access_token')};
        return {url: process.env.REACT_APP_ENDPOINT + 'applicant/attachment/upload', body: formData, headers: hdr};
    }

    handleChangeStatus = ({ meta, remove }, status) => {
        if (status === 'headers_received') {
            this.setState({fileName: meta.name});
            remove();
        } else if (status === 'aborted') {
            this.state.fileName = '';
        }
    }

    closeAlert = () => {
        this.setState({
            showAlert: false
        });
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.selectedUser !== '' && nextProps.selectedUser !== undefined && this.state.regEmailAddress !== nextProps.selectedUser){
            this.state.regEmailAddress = nextProps.selectedUser;

            const options = {
                headers: { 'access-token': localStorage.getItem('access_token')}
            };
            axios.get(process.env.REACT_APP_ENDPOINT + `applicant/attachments?regEmailAddress=${nextProps.selectedUser}`, options).then(res => {
                if(res.status === 200) {
                    this.setState({
                        attachments: res.data
                    })
                } else if(res.status === 1015) {

                }
            }).catch(error => {
                console.log(error);
            });
        }
    }

    downloadAllAtachment = () => {
        const options = {
                headers: { 'access-token': localStorage.getItem('access_token')}
        };
        const url = process.env.REACT_APP_ENDPOINT + `applicant/attachment/all?regEmailAddress=${this.state.regEmailAddress}`;
        axios.get(url, options).then(res => {
            if(res.status === 200) {
               console.log(res.data);
               window.open(res.data, '_blank');
            } else if(res.status === 1015) {

            }
        }).catch(error => {
            console.log(error);
        });
       // window.open(url, '_blank');
    }

    render(){
        return (
            <Fragment>
                <SweetAlert
                    show={this.state.showAlert}
                    type={this.state.basicAlertType}
                    title={this.state.basicAlertTitle}
                    onConfirm={this.closeAlert}
                />
                <div className="card theme-form theme-from-scroll">
                    <div className="card-body">
                        <div className="form-row">
                            <div className="col-sm-12">
                                <form className="dropzone digits" id="fileTypeValidation">
                                    <div className="dz-message needsclick">
                                        <Dropzone
                                            getUploadParams={this.getUploadParams}
                                            onChangeStatus={this.handleChangeStatus}
                                            maxFiles={1}
                                            multiple={false}
                                            canCancel={false}
                                            inputContent="Drop A File"
                                            styles={{
                                            dropzone: { height: 10 },
                                            dropzoneActive: { borderColor: 'green' },
                                            }}
                                        />
                                    </div>
                                    <div className="form-row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label className="upload-file-name">{this.state.fileName}</label>
                                                <input name="description" className="form-control my-4"
                                                    placeholder="Description" onChange={this.handleChange} />
                                            </div>
                                        </div>
                                    </div>                   
                                    <div className="form-row">
                                        <div className="col-md-1 mr-3">
                                            <Button variant="primary" onClick={this.handleSave}>Save </Button>
                                        </div> 
                                        <div className="col-md-1 ml-3 mr-3">
                                            <Button variant="primary" onClick={() => this.downloadAllAtachment()} style={{ marginBottom: '1rem' }}>
                                            <Download className="menuIcon"/> All Atachment</Button>                             
                                        </div>                             
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="form-row p-t-15">
                            <div className="col-sm-12">
                                <TableModel data={this.state.attachments} columns={this.state.columns} addSelectedRowObj={this.addSelectedRowObj} 
                                        removeUnSelectedRowObj={this.removeUnSelectedRowObj}/>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default Attachments;