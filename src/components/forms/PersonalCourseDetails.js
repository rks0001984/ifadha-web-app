import React, {Component, Fragment} from 'react';
import { Button } from 'reactstrap';
import axios from 'axios';
import DatePicker from "react-datepicker";
import SweetAlert from 'react-bootstrap-sweetalert';
import "react-datepicker/dist/react-datepicker.css";


class PersonalCourseDetails extends Component {

    constructor(props){
        super(props);
        this.state = {
            regEmailAddress: '',
            alternateStartDate: new Date(),
            alternateEndDate: new Date(),
            courseEndDate: new Date(),
            courseStartDate: new Date(),
            birthCountry: '',
            courseTitle: '',
            dateOfBirth: new Date(),
            disability: '',
            emailAddress: '',
            ethinicity: '',
            faxNumber: '',
            niNumber: '',
            firstName: '',
            gender: '',
            intake: '',
            lastName: '',
            maritalStatus: '',
            middleName: '',
            mobileNumber: '',
            nationality: '',
            studentId: 0,
            studentType: '',
            telDay: '',
            telEvening: '',
            title: '',
            wishToStartStudiesDate: new Date(),
            applicant: {},
            showAlert: false,
            basicAlertTitle:'',
            basicAlertType:"default"  
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.selectedUser !== '' && nextProps.selectedUser !== undefined && this.state.regEmailAddress !== nextProps.selectedUser){
            this.setState({
                regEmailAddress: nextProps.selectedUser,
                emailAddress: nextProps.selectedUser
            });
            const options = {
                headers: { 'access-token': localStorage.getItem('access_token')}
            };
            axios.get(process.env.REACT_APP_ENDPOINT + `applicant/personalAndCourseDetails?regEmailAddress=${nextProps.selectedUser}`, options)
            .then(res => {
                const data = res.data;
                this.setState({
                    regEmailAddress: nextProps.selectedUser,
                    alternateStartDate: data.alternateStartDate == null ? new Date() : new Date(data.alternateStartDate),
                    alternateEndDate: data.alternateEndDate == null ? new Date() : new Date(data.alternateEndDate),
                    courseEndDate: data.courseEndDate == null ? new Date() : new Date(data.courseEndDate),
                    courseStartDate: data.courseStartDate == null ? new Date() : new Date(data.courseStartDate),
                    birthCountry: data.birthCountry,
                    courseTitle: data.courseTitle,
                    dateOfBirth: data.dateOfBirth == null ? new Date() : new Date(data.dateOfBirth),
                    disability: data.disability,
                    emailAddress: data.emailAddress,
                    ethinicity: data.ethinicity,
                    faxNumber: data.faxNumber,
                    firstName: data.firstName,
                    gender: data.gender,
                    intake: data.intake,
                    lastName: data.lastName,
                    maritalStatus: data.maritalStatus,
                    middleName: data.middleName,
                    mobileNumber: data.mobileNumber,
                    nationality: data.nationality,
                    studentId: data.studentId,
                    studentType: data.studentType,
                    telDay: data.telDay,
                    telEvening: data.telEvening,
                    title: data.title,
                    wishToStartStudiesDate: data.wishToStartStudiesDate == null ? new Date() : new Date(data.wishToStartStudiesDate),
                    niNumber: data.niNumber
                });
            }).catch(error => {
                console.log(error)
            });
        }
    }

    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        this.setState({[name]: value});
    }

    handleSave = () => { 
        const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };
        axios.post(process.env.REACT_APP_ENDPOINT + 'applicant/personalAndCourseDetails/create-update', {
            regEmailAddress: this.state.regEmailAddress,
            alternateEndDate: this.state.alternateEndDate,
            alternateStartDate: this.state.alternateStartDate,
            birthCountry: this.state.birthCountry,
            courseEndDate: this.state.courseEndDate,
            courseStartDate: this.state.courseStartDate,
            courseTitle: this.state.courseTitle,
            dateOfBirth: this.state.dateOfBirth,
            disability: this.state.disability,
            emailAddress: this.state.emailAddress,
            ethinicity: this.state.ethinicity,
            faxNumber: this.state.faxNumber,
            firstName: this.state.firstName,
            gender: this.state.gender,
            intake: this.state.intake,
            lastName: this.state.lastName,
            maritalStatus: this.state.maritalStatus,
            middleName: this.state.middleName,
            mobileNumber: this.state.mobileNumber,
            nationality: this.state.nationality,
            studentType: this.state.studentType,
            telDay: this.state.telDay,
            telEvening: this.state.telEvening,
            title: this.state.title,
            wishToStartStudiesDate: this.state.wishToStartStudiesDate,
            niNumber: this.state.niNumber
        }, options).then(res => {
            if(res.status === 200) {
                this.setState({
                    showAlert: true,
                    basicAlertTitle: 'Personal Course Details save successfully!!!',
                    basicAlertType: "success"
                })
            }
        }).catch(error => {
            this.setState({
                showAlert: true,
                basicAlertTitle: 'Unbale to save Personal Course Details!',
                basicAlertType: "error"
            })
        });
    }

    closeAlert = () => {
        this.setState({
            showAlert: false
        });
    }

    dobChanged = date => {
        this.setState({
            dateOfBirth: date
        })
    }

    wishToStartStudiesDateChange = date => {
        this.setState({
            wishToStartStudiesDate: date
        })
    }

    courseStartDateChange = date => {
        this.setState({
            courseStartDate: date
        })
    }

    courseEndDateChange = date => {
        this.setState({
            courseEndDate: date
        })
    }

    alternateStartDateChange = date => {
        this.setState({
            alternateStartDate: date
        })
    }

    alternateEndDateChange = date => {
        this.setState({
            alternateEndDate: date
        })
    }

    render(){
        return (
            <Fragment>
                <SweetAlert
                    show={this.state.showAlert}
                    type={this.state.basicAlertType}
                    title={this.state.basicAlertTitle}
                    onConfirm={this.closeAlert}
                />
                <form className="theme-form theme-from-scroll p-4" id="personalCourseDetailsForm" >
                    <div className="form-row">
                        <div className="category-label">
                            <label>Personal Details</label>
                        </div>
                    </div>
                    <div className="card card-border-top">
                        <div className="card-body">
                            <div className="form-row">
                                <div className="col-md-2">
                                    <div className="form-group">
                                        <label className="col-form-label">Title</label> 
                                        <select className="form-control" name="title" value={this.state.title} onChange={this.handleChange} >
                                            <option value="--Select--">--Select--</option>
                                            <option value="Mr">Mr</option>
                                            <option value="Dr">Dr</option>
                                            <option value="Mrs">Mrs</option>
                                            <option value="Miss">Miss</option>
                                            <option value="Ms">Ms</option>
                                            <option value="Other">Other</option>
                                        </select>                                      
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">First Name</label>
                                        <input name="firstName" className="form-control"
                                            value={this.state.firstName}
                                            placeholder="Your First Name" onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Middle Name</label>
                                        <input name="middleName" className="form-control"
                                            placeholder="Your Middle Name" value={this.state.middleName} onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Last Name</label>
                                        <input name="lastName" className="form-control"
                                            placeholder="Your Last Name" value={this.state.lastName} onChange={this.handleChange} />
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Gender</label>
                                        <select className="form-control" name="gender" 
                                            value={this.state.gender} onChange={this.handleChange} >
                                            <option value="--Select">--Select--</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Marital Status</label>
                                        <select className="form-control" name="maritalStatus" 
                                            value={this.state.maritalStatus} onChange={this.handleChange} >
                                            <option value="Married">Married</option>
                                            <option value="UnMarried">UnMarried</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">DOB</label>
                                        <DatePicker name="dateOfBirth" className="form-control digits" selected={this.state.dateOfBirth} onChange={this.dobChanged} />
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Email</label>
                                        <input name="emailAddress" className="form-control"
                                            placeholder="Your Email" value={this.state.emailAddress} onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Nationality</label>
                                        <input name="nationality" className="form-control"
                                            placeholder="Your Nationality" value={this.state.nationality} onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Birth Country</label>
                                        <select className="form-control" name="birthCountry" 
                                            value={this.state.birthCountry} onChange={this.handleChange} >
                                            <option value="Afganistan">Afghanistan</option>
                                            <option value="Albania">Albania</option>
                                            <option value="Algeria">Algeria</option>
                                            <option value="American Samoa">American Samoa</option>
                                            <option value="Andorra">Andorra</option>
                                            <option value="Angola">Angola</option>
                                            <option value="Anguilla">Anguilla</option>
                                            <option value="Antigua & Barbuda">Antigua & Barbuda</option>
                                            <option value="Argentina">Argentina</option>
                                            <option value="Armenia">Armenia</option>
                                            <option value="Aruba">Aruba</option>
                                            <option value="Australia">Australia</option>
                                            <option value="Austria">Austria</option>
                                            <option value="Azerbaijan">Azerbaijan</option>
                                            <option value="Bahamas">Bahamas</option>
                                            <option value="Bahrain">Bahrain</option>
                                            <option value="Bangladesh">Bangladesh</option>
                                            <option value="Barbados">Barbados</option>
                                            <option value="Belarus">Belarus</option>
                                            <option value="Belgium">Belgium</option>
                                            <option value="Belize">Belize</option>
                                            <option value="Benin">Benin</option>
                                            <option value="Bermuda">Bermuda</option>
                                            <option value="Bhutan">Bhutan</option>
                                            <option value="Bolivia">Bolivia</option>
                                            <option value="Bonaire">Bonaire</option>
                                            <option value="Bosnia & Herzegovina">Bosnia & Herzegovina</option>
                                            <option value="Botswana">Botswana</option>
                                            <option value="Brazil">Brazil</option>
                                            <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                                            <option value="Brunei">Brunei</option>
                                            <option value="Bulgaria">Bulgaria</option>
                                            <option value="Burkina Faso">Burkina Faso</option>
                                            <option value="Burundi">Burundi</option>
                                            <option value="Cambodia">Cambodia</option>
                                            <option value="Cameroon">Cameroon</option>
                                            <option value="Canada">Canada</option>
                                            <option value="Canary Islands">Canary Islands</option>
                                            <option value="Cape Verde">Cape Verde</option>
                                            <option value="Cayman Islands">Cayman Islands</option>
                                            <option value="Central African Republic">Central African Republic</option>
                                            <option value="Chad">Chad</option>
                                            <option value="Channel Islands">Channel Islands</option>
                                            <option value="Chile">Chile</option>
                                            <option value="China">China</option>
                                            <option value="Christmas Island">Christmas Island</option>
                                            <option value="Cocos Island">Cocos Island</option>
                                            <option value="Colombia">Colombia</option>
                                            <option value="Comoros">Comoros</option>
                                            <option value="Congo">Congo</option>
                                            <option value="Cook Islands">Cook Islands</option>
                                            <option value="Costa Rica">Costa Rica</option>
                                            <option value="Cote DIvoire">Cote DIvoire</option>
                                            <option value="Croatia">Croatia</option>
                                            <option value="Cuba">Cuba</option>
                                            <option value="Curaco">Curacao</option>
                                            <option value="Cyprus">Cyprus</option>
                                            <option value="Czech Republic">Czech Republic</option>
                                            <option value="Denmark">Denmark</option>
                                            <option value="Djibouti">Djibouti</option>
                                            <option value="Dominica">Dominica</option>
                                            <option value="Dominican Republic">Dominican Republic</option>
                                            <option value="East Timor">East Timor</option>
                                            <option value="Ecuador">Ecuador</option>
                                            <option value="Egypt">Egypt</option>
                                            <option value="El Salvador">El Salvador</option>
                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                            <option value="Eritrea">Eritrea</option>
                                            <option value="Estonia">Estonia</option>
                                            <option value="Ethiopia">Ethiopia</option>
                                            <option value="Falkland Islands">Falkland Islands</option>
                                            <option value="Faroe Islands">Faroe Islands</option>
                                            <option value="Fiji">Fiji</option>
                                            <option value="Finland">Finland</option>
                                            <option value="France">France</option>
                                            <option value="French Guiana">French Guiana</option>
                                            <option value="French Polynesia">French Polynesia</option>
                                            <option value="French Southern Ter">French Southern Ter</option>
                                            <option value="Gabon">Gabon</option>
                                            <option value="Gambia">Gambia</option>
                                            <option value="Georgia">Georgia</option>
                                            <option value="Germany">Germany</option>
                                            <option value="Ghana">Ghana</option>
                                            <option value="Gibraltar">Gibraltar</option>
                                            <option value="Great Britain">Great Britain</option>
                                            <option value="Greece">Greece</option>
                                            <option value="Greenland">Greenland</option>
                                            <option value="Grenada">Grenada</option>
                                            <option value="Guadeloupe">Guadeloupe</option>
                                            <option value="Guam">Guam</option>
                                            <option value="Guatemala">Guatemala</option>
                                            <option value="Guinea">Guinea</option>
                                            <option value="Guyana">Guyana</option>
                                            <option value="Haiti">Haiti</option>
                                            <option value="Hawaii">Hawaii</option>
                                            <option value="Honduras">Honduras</option>
                                            <option value="Hong Kong">Hong Kong</option>
                                            <option value="Hungary">Hungary</option>
                                            <option value="Iceland">Iceland</option>
                                            <option value="Indonesia">Indonesia</option>
                                            <option value="India">India</option>
                                            <option value="Iran">Iran</option>
                                            <option value="Iraq">Iraq</option>
                                            <option value="Ireland">Ireland</option>
                                            <option value="Isle of Man">Isle of Man</option>
                                            <option value="Israel">Israel</option>
                                            <option value="Italy">Italy</option>
                                            <option value="Jamaica">Jamaica</option>
                                            <option value="Japan">Japan</option>
                                            <option value="Jordan">Jordan</option>
                                            <option value="Kazakhstan">Kazakhstan</option>
                                            <option value="Kenya">Kenya</option>
                                            <option value="Kiribati">Kiribati</option>
                                            <option value="Korea North">Korea North</option>
                                            <option value="Korea Sout">Korea South</option>
                                            <option value="Kuwait">Kuwait</option>
                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                            <option value="Laos">Laos</option>
                                            <option value="Latvia">Latvia</option>
                                            <option value="Lebanon">Lebanon</option>
                                            <option value="Lesotho">Lesotho</option>
                                            <option value="Liberia">Liberia</option>
                                            <option value="Libya">Libya</option>
                                            <option value="Liechtenstein">Liechtenstein</option>
                                            <option value="Lithuania">Lithuania</option>
                                            <option value="Luxembourg">Luxembourg</option>
                                            <option value="Macau">Macau</option>
                                            <option value="Macedonia">Macedonia</option>
                                            <option value="Madagascar">Madagascar</option>
                                            <option value="Malaysia">Malaysia</option>
                                            <option value="Malawi">Malawi</option>
                                            <option value="Maldives">Maldives</option>
                                            <option value="Mali">Mali</option>
                                            <option value="Malta">Malta</option>
                                            <option value="Marshall Islands">Marshall Islands</option>
                                            <option value="Martinique">Martinique</option>
                                            <option value="Mauritania">Mauritania</option>
                                            <option value="Mauritius">Mauritius</option>
                                            <option value="Mayotte">Mayotte</option>
                                            <option value="Mexico">Mexico</option>
                                            <option value="Midway Islands">Midway Islands</option>
                                            <option value="Moldova">Moldova</option>
                                            <option value="Monaco">Monaco</option>
                                            <option value="Mongolia">Mongolia</option>
                                            <option value="Montserrat">Montserrat</option>
                                            <option value="Morocco">Morocco</option>
                                            <option value="Mozambique">Mozambique</option>
                                            <option value="Myanmar">Myanmar</option>
                                            <option value="Nambia">Nambia</option>
                                            <option value="Nauru">Nauru</option>
                                            <option value="Nepal">Nepal</option>
                                            <option value="Netherland Antilles">Netherland Antilles</option>
                                            <option value="Netherlands">Netherlands (Holland, Europe)</option>
                                            <option value="Nevis">Nevis</option>
                                            <option value="New Caledonia">New Caledonia</option>
                                            <option value="New Zealand">New Zealand</option>
                                            <option value="Nicaragua">Nicaragua</option>
                                            <option value="Niger">Niger</option>
                                            <option value="Nigeria">Nigeria</option>
                                            <option value="Niue">Niue</option>
                                            <option value="Norfolk Island">Norfolk Island</option>
                                            <option value="Norway">Norway</option>
                                            <option value="Oman">Oman</option>
                                            <option value="Pakistan">Pakistan</option>
                                            <option value="Palau Island">Palau Island</option>
                                            <option value="Palestine">Palestine</option>
                                            <option value="Panama">Panama</option>
                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                            <option value="Paraguay">Paraguay</option>
                                            <option value="Peru">Peru</option>
                                            <option value="Phillipines">Philippines</option>
                                            <option value="Pitcairn Island">Pitcairn Island</option>
                                            <option value="Poland">Poland</option>
                                            <option value="Portugal">Portugal</option>
                                            <option value="Puerto Rico">Puerto Rico</option>
                                            <option value="Qatar">Qatar</option>
                                            <option value="Republic of Montenegro">Republic of Montenegro</option>
                                            <option value="Republic of Serbia">Republic of Serbia</option>
                                            <option value="Reunion">Reunion</option>
                                            <option value="Romania">Romania</option>
                                            <option value="Russia">Russia</option>
                                            <option value="Rwanda">Rwanda</option>
                                            <option value="St Barthelemy">St Barthelemy</option>
                                            <option value="St Eustatius">St Eustatius</option>
                                            <option value="St Helena">St Helena</option>
                                            <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                                            <option value="St Lucia">St Lucia</option>
                                            <option value="St Maarten">St Maarten</option>
                                            <option value="St Pierre & Miquelon">St Pierre & Miquelon</option>
                                            <option value="St Vincent & Grenadines">St Vincent & Grenadines</option>
                                            <option value="Saipan">Saipan</option>
                                            <option value="Samoa">Samoa</option>
                                            <option value="Samoa American">Samoa American</option>
                                            <option value="San Marino">San Marino</option>
                                            <option value="Sao Tome & Principe">Sao Tome & Principe</option>
                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                            <option value="Senegal">Senegal</option>
                                            <option value="Seychelles">Seychelles</option>
                                            <option value="Sierra Leone">Sierra Leone</option>
                                            <option value="Singapore">Singapore</option>
                                            <option value="Slovakia">Slovakia</option>
                                            <option value="Slovenia">Slovenia</option>
                                            <option value="Solomon Islands">Solomon Islands</option>
                                            <option value="Somalia">Somalia</option>
                                            <option value="South Africa">South Africa</option>
                                            <option value="Spain">Spain</option>
                                            <option value="Sri Lanka">Sri Lanka</option>
                                            <option value="Sudan">Sudan</option>
                                            <option value="Suriname">Suriname</option>
                                            <option value="Swaziland">Swaziland</option>
                                            <option value="Sweden">Sweden</option>
                                            <option value="Switzerland">Switzerland</option>
                                            <option value="Syria">Syria</option>
                                            <option value="Tahiti">Tahiti</option>
                                            <option value="Taiwan">Taiwan</option>
                                            <option value="Tajikistan">Tajikistan</option>
                                            <option value="Tanzania">Tanzania</option>
                                            <option value="Thailand">Thailand</option>
                                            <option value="Togo">Togo</option>
                                            <option value="Tokelau">Tokelau</option>
                                            <option value="Tonga">Tonga</option>
                                            <option value="Trinidad & Tobago">Trinidad & Tobago</option>
                                            <option value="Tunisia">Tunisia</option>
                                            <option value="Turkey">Turkey</option>
                                            <option value="Turkmenistan">Turkmenistan</option>
                                            <option value="Turks & Caicos Is">Turks & Caicos Is</option>
                                            <option value="Tuvalu">Tuvalu</option>
                                            <option value="Uganda">Uganda</option>
                                            <option value="United Kingdom">United Kingdom</option>
                                            <option value="Ukraine">Ukraine</option>
                                            <option value="United Arab Erimates">United Arab Emirates</option>
                                            <option value="United States of America">United States of America</option>
                                            <option value="Uraguay">Uruguay</option>
                                            <option value="Uzbekistan">Uzbekistan</option>
                                            <option value="Vanuatu">Vanuatu</option>
                                            <option value="Vatican City State">Vatican City State</option>
                                            <option value="Venezuela">Venezuela</option>
                                            <option value="Vietnam">Vietnam</option>
                                            <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                                            <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                                            <option value="Wake Island">Wake Island</option>
                                            <option value="Wallis & Futana Is">Wallis & Futana Is</option>
                                            <option value="Yemen">Yemen</option>
                                            <option value="Zaire">Zaire</option>
                                            <option value="Zambia">Zambia</option>
                                            <option value="Zimbabwe">Zimbabwe</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Tel Day</label>
                                        <input name="telDay" className="form-control"
                                            placeholder="Tel Day" value={this.state.telDay} onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Tel Evening</label>
                                        <input name="telEvening" className="form-control"
                                            placeholder="Tel Evening" value={this.state.telEvening} onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Mobile</label>
                                        <input name="mobileNumber" className="form-control"
                                            placeholder="Your Mobile Number" value={this.state.mobileNumber} onChange={this.handleChange} />
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Fax</label>
                                        <input name="faxNumber" className="form-control"
                                            placeholder="Your Fax Number" value={this.state.faxNumber} onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">NI Number</label>
                                        <input name="niNumber" className="form-control"
                                            placeholder="Your NI Number" value={this.state.niNumber} onChange={this.handleChange} />
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Student Type</label>
                                        <select className="form-control" name="studentType" 
                                            value={this.state.studentType} onChange={this.handleChange} >
                                            <option value="Local">Local</option>
                                            <option value="Foreigner">Foreigner</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Disability</label>
                                        <select className="form-control" name="disability" 
                                            value={this.state.disability} onChange={this.handleChange} >
                                            <option value="--Select--">--Select--</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Ethnicity</label>
                                        <select className="form-control" name="ethinicity" 
                                            value={this.state.ethinicity} onChange={this.handleChange} >
                                            <option value="--Select--">--Select--</option>
                                            <option value="White">White</option>
                                            <option value="White - (31) English/Welsh/Scots/Northern Irish/British">White - (31) English/Welsh/Scots/Northern Irish/British</option>
                                            <option value="White - (32) Irish">White - (32) Irish</option>
                                            <option value="White - (33) Gypsy or Irish Traveller">White - (33) Gypsy or Irish Traveller</option>
                                            <option value="White - (44) Any Other White Background">White - (44) Any Other White Background</option>
                                            <option value="Mixed - (35) White and Black Caribbean">White - (35) White and Black Caribbean</option>
                                            <option value="Mixed - (36) White Black African">Mixed - (36) White Black African</option>
                                            <option value="Mixed - (37) White Asian">Mixed - (37) White Asian</option>
                                            <option value="Mixed - (38) Any Other Mixed Multiple Background">Mixed - (38) Any Other Mixed Multiple Background</option>
                                            <option value="Asian - (39) Indian">Asian - (39) Indian</option>
                                            <option value="Asian - (40) Pakistani">Asian - (40) Pakistani</option>
                                            <option value="Asian - (41) Bangladesi">Asian - (41) Bangladesi</option>
                                            <option value="Asian - (42) Chinese">Asian - (42) Chinese</option>
                                            <option value="Asian - (43) Any Other Asian Background">Asian - (43) Any Other Asian Background</option>
                                            <option value="Black/African/Caribbean/Black British - (44) African">Black/African/Caribbean/Black British - (44) African</option>
                                            <option value="Black/African/Caribbean/Black British - (45) Caribbean">Black/African/Caribbean/Black British - (45) Caribbean</option>
                                            <option value="Black/African/Caribbean/Black British - (46) Any Other Black Background">Black/African/Caribbean/Black British - (46) Any Other Black Background</option>
                                            <option value="Black/African/Caribbean/Black British - (47) Arab">Black/African/Caribbean/Black British - (47) Arab</option>
                                            <option value="Prefer not to Disclose">Prefer not to Disclose</option>
                                            <option value="Not Known">Not Known</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="category-label">
                            <label>Course Details</label>
                        </div>
                    </div>
                    <div className="card card-border-top">
                        <div className="card-body">
                            <div className="form-row">
                                <div className="col-md-12">
                                    <div className="form-group">
                                        <label className="col-form-label">Course Title</label>
                                        <select className="form-control" name="courseTitle" 
                                            value={this.state.courseTitle} onChange={this.handleChange} >
                                            <option value="--Select--">--Select--</option>
                                            <option value="DIPLOMA IN FOOD AND BEVERAGE">DIPLOMA IN FOOD AND BEVERAGE</option>
                                            <option value="DIPLOMA IN IT APPRENTICESHIP">DIPLOMA IN IT APPRENTICESHIP</option>
                                            <option value="DIPLOMA IN PROFESSIONAL COOKERY">DIPLOMA IN PROFESSIONAL COOKERY</option>
                                            <option value="FUNCTIONAL SKILLS ENGLISH">FUNCTIONAL SKILLS ENGLISH</option>
                                            <option value="FUNCTIONAL SKILLS MATHEMATICS">FUNCTIONAL SKILLS MATHEMATICS</option>
                                            <option value="IT APPRENTICESHIP">IT APPRENTICESHIP</option>
                                            <option value="LEVEL 3 DIPLOMA IN BUSINESS">LEVEL 3 DIPLOMA IN BUSINESS</option>
                                            <option value="PVT FUNCTIONAL SKILLS ENGLISH">PVT FUNCTIONAL SKILLS ENGLISH</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-12">
                                    <div className="form-group">
                                        <label className="col-form-label">Intake</label>
                                        <select className="form-control" name="intake" 
                                            value={this.state.intake} onChange={this.handleChange} >
                                            <option value="Intake One">Intake One</option>
                                            <option value="Intake Two">Intake Two</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">When do you want to start your studies:</label>
                                        <DatePicker name="wishToStartStudiesDate" className="form-control digits" selected={this.state.wishToStartStudiesDate} onChange={this.wishToStartStudiesDateChange} />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Course Start Date:</label>
                                        <DatePicker name="courseStartDate" className="form-control digits" selected={this.state.courseStartDate} onChange={this.courseStartDateChange} />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Course End Date:</label>
                                        <DatePicker name="courseEndDate" className="form-control digits" selected={this.state.courseEndDate} onChange={this.courseEndDateChange} />
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Alternate Start Date:</label>
                                        <DatePicker className="form-control digits" name="alternateStartDate" selected={this.state.alternateStartDate} onChange={this.alternateStartDateChange} />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Alternate End Date:</label>
                                        <DatePicker name="alternateEndDate" className="form-control digits" selected={this.state.alternateEndDate} onChange={this.alternateEndDateChange} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-1 mr-3">
                        <Button variant="primary" onClick={this.handleSave}>Save </Button>
                    </div>                               
                </form>
            </Fragment>
        )
    }
}

export default PersonalCourseDetails;