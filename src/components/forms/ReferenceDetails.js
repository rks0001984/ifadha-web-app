import React, {Component, Fragment} from 'react';
import axios from 'axios';
import { Button } from 'reactstrap';
import SweetAlert from 'react-bootstrap-sweetalert';

class ReferenceDetails extends Component {
    constructor(props){
        super(props);
        this.state = {
            regEmailAddress: '',
            refNo: '',
            showAlert: false,
            basicAlertTitle:'',
            basicAlertType:"default",
            isStudent: localStorage.getItem('access_role') === 'STUDENT',
            createdBy:''
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.selectedUser !== '' && nextProps.selectedUser !== undefined && this.state.regEmailAddress !== nextProps.selectedUser){
            this.setState({
                regEmailAddress: nextProps.selectedUser,
                emailAddress: nextProps.selectedUser
            });
            const options = {
                headers: { 'access-token': localStorage.getItem('access_token')}
            };
            axios.get(process.env.REACT_APP_ENDPOINT + `applicant/applicantReference?regEmailAddress=${nextProps.selectedUser}`, options)
            .then(res => {
                if(res.status === 200) {
                    const data = res.data;
                    this.setState({
                        refNo: res.data.refNo,
                        regEmailAddress: nextProps.selectedUser,
                        createdBy: data.createdBy
                    });
                    this.updateRefNumber(res.data.refNo);
                }
            }).catch(error => {
                console.log(error)
            });
        }
    }

    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        this.setState({[name]: value});
    }

    handleSave = () => {
         const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };
        axios.post(process.env.REACT_APP_ENDPOINT + 'applicant/applicantReference/create-update', {
            refNo: this.state.refNo,
            regEmailAddress: this.state.regEmailAddress
        }, options).then(res => {
            if(res.status === 200) {
                this.updateRefNumber(this.state.refNo);
                this.setState({
                    showAlert: true,
                    basicAlertTitle: 'Reference number save successfully!!!',
                    basicAlertType: "success"
                })
            }
        }).catch(error => {
            this.setState({
                showAlert: true,
                basicAlertTitle: 'Unbale to save Reference number!',
                basicAlertType: "error"
            })
        });
    }

    closeAlert = () => {
        this.setState({
            showAlert: false
        });
    }

    updateRefNumber = refNumber => {
        if(refNumber !== null || refNumber !== undefined){
            const{ updateReferenceNumber } = this.props;
            updateReferenceNumber(refNumber);
        }        
    }
    
    render(){
        return (
            <Fragment>
                <SweetAlert
                    show={this.state.showAlert}
                    type={this.state.basicAlertType}
                    title={this.state.basicAlertTitle}
                    onConfirm={this.closeAlert}
                />
                <form className="theme-form theme-from-scroll p-4" id="referenceDetailsFrom" >
                    <div className="form-row">
                        <div className="form-group pr-2">
                            <label>Reference number:</label>
                        </div>
                        <div className="form-group">
                          <input type="text" className="form-control" disabled={this.state.isStudent} id="refNo" placeholder="Enter Reference number" name="refNo"
                                value={this.state.refNo} onChange={this.handleChange}/>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group pr-5">
                        <label>Created By: <b>{this.state.createdBy}</b></label>
                        </div>
                    </div>
                    <div className="form-row" hidden={this.state.isStudent}>
                        <div className="col-md-1 mr-3">
                            <Button variant="primary" onClick={this.handleSave}>Save </Button>
                        </div>                             
                    </div>                        
                </form>
            </Fragment>
        )
    }
}

export default ReferenceDetails;