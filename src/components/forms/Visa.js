import React, {Component, Fragment} from 'react';
import { Button } from 'reactstrap';
import axios from 'axios';
import SweetAlert from 'react-bootstrap-sweetalert';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

class Visa extends Component {

    constructor(props){
        super(props);
        this.state = {
            showAlert: false,
            basicAlertTitle: '',
            basicAlertType: "default",
            regEmailAddress: '',
            casCode: "",
            casIssued: "",
            casObtained: "",
            casWithdrawn: "",
            createdBy: "",
            createdDate: new Date(),
            dateAssigned: new Date(),
            drivingLicenceNo: "",
            expiryDate: new Date(),
            idCardExpiry: new Date(),
            idCardNumber: "",
            modifiedDate: new Date(),
            passportExpiry: new Date(),
            passportIssue: new Date(),
            passportNumber: "",
            visaExpiry: new Date(),
            visaRequired: "",
            visaStatus: "",
            visaValidFrom: new Date()
        }
    }

    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        this.setState({[name]: value});
    }

    visaValidFromChange = date => {
        this.setState({
            visaValidFrom: date
        })
    }

    visaExpiryChange = date => {
        this.setState({
            visaExpiry: date
        })
    }

    passportIssueChange = date => {
        this.setState({
            passportIssue: date
        })
    }

    passportExpiryChange = date => {
        this.setState({
            passportExpiry: date
        })
    }

    idCardExpiryChange = date => {
        this.setState({
            idCardExpiry: date
        })
    }

    dateAssignedChange = date => {
        this.setState({
            dateAssigned: date
        })
    }

    expiryDateChange = date => {
        this.setState({
            expiryDate: date
        })
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.selectedUser !== '' && nextProps.selectedUser !== undefined && this.state.regEmailAddress !== nextProps.selectedUser){
            this.setState({
                regEmailAddress: nextProps.selectedUser
            });
            const options = {
                headers: { 'access-token': localStorage.getItem('access_token')}
            };
            axios.get(process.env.REACT_APP_ENDPOINT + `applicant/visaDetails?regEmailAddress=${nextProps.selectedUser}`, options)
            .then(res => {
                const data = res.data;
                this.setState({
                    regEmailAddress: nextProps.selectedUser,
                    casCode: data.casCode,
                    casIssued: data.casIssued,
                    casObtained: data.casObtained,
                    casWithdrawn: data.casWithdrawn,
                    createdBy: data.createdBy,
                    createdDate: data.createdDate == null ? new Date() : new Date(data.createdDate),
                    dateAssigned: data.dateAssigned == null ? new Date() : new Date(data.dateAssigned),
                    drivingLicenceNo: data.drivingLicenceNo,
                    expiryDate: data.expiryDate == null ? new Date() : new Date(data.expiryDate),
                    idCardExpiry: data.idCardExpiry == null ? new Date() : new Date(data.idCardExpiry),
                    idCardNumber: data.idCardNumber,
                    modifiedDate: data.modifiedDate == null ? new Date() : new Date(data.modifiedDate),
                    passportExpiry: data.passportExpiry == null ? new Date() : new Date(data.passportExpiry),
                    passportIssue: data.passportIssue == null ? new Date() : new Date(data.passportIssue),
                    passportNumber: data.passportNumber,
                    visaExpiry: data.visaExpiry == null ? new Date() : new Date(data.visaExpiry),
                    visaRequired: data.visaRequired,
                    visaStatus: data.visaStatus,
                    visaValidFrom: data.visaValidFrom == null ? new Date() : new Date(data.visaValidFrom)
                });
            }).catch(error => {
                console.log(error)
            });
        }
    }

    handleSave = () => { 
        const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };
        axios.post(process.env.REACT_APP_ENDPOINT + 'applicant/visaDetails/create-update', {
            casCode: this.state.casCode,
            casIssued: this.state.casIssued,
            casObtained: this.state.casObtained,
            casWithdrawn: this.state.casWithdrawn,
            createdBy: this.state.createdBy,
            dateAssigned: this.state.dateAssigned,
            drivingLicenceNo: this.state.drivingLicenceNo,
            expiryDate: this.state.expiryDate,
            idCardExpiry: this.state.idCardExpiry,
            idCardNumber: this.state.idCardNumber,
            passportExpiry: this.state.passportExpiry,
            passportIssue: this.state.passportIssue,
            passportNumber: this.state.passportNumber,
            regEmailAddress: this.state.regEmailAddress,
            studentId: this.state.studentId,
            visaExpiry: this.state.visaExpiry,
            visaRequired: this.state.visaRequired,
            visaStatus: this.state.visaStatus,
            visaValidFrom: this.state.visaValidFrom
        }, options).then(res => {
            if(res.status === 200) {
                this.setState({
                    showAlert: true,
                    basicAlertTitle: 'Visa Details save successfully!!!',
                    basicAlertType: "success"
                })
            }
        }).catch(error => {
            this.setState({
                showAlert: true,
                basicAlertTitle: 'Unbale to save Visa Details!',
                basicAlertType: "error"
            })
        });
    }

    closeAlert = () => {
        this.setState({
            showAlert: false
        });
    }

    render(){
        return (
            <Fragment>
                <SweetAlert
                    show={this.state.showAlert}
                    type={this.state.basicAlertType}
                    title={this.state.basicAlertTitle}
                    onConfirm={this.closeAlert}
                />
                <form className="theme-form theme-from-scroll p-4" id="visaForm" >
                    <div className="form-row">
                        <div className="category-label">
                            <label>Visa Requirements</label>
                        </div>
                    </div>
                    <div className="card card-border-top">
                        <div className="form-row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="col-form-label">Visa Required</label>
                                    <select className="form-control" name="visaRequired" 
                                        value={this.state.visaRequired} onChange={this.handleChange} >
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="col-form-label">Visa Status</label>
                                    <select className="form-control" name="visaStatus" 
                                        value={this.state.visaStatus} onChange={this.handleChange} >
                                        <option value="Active">Active</option>
                                        <option value="Inactive">Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="col-form-label">Visa Valid From</label>
                                    <DatePicker name="visaValidFrom" className="form-control digits" 
                                        selected={this.state.visaValidFrom} onChange={this.visaValidFromChange} />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="col-form-label">Visa Expiry</label>
                                    <DatePicker name="visaExpiry" className="form-control digits" 
                                        selected={this.state.visaExpiry} onChange={this.visaExpiryChange} />
                                </div>
                            </div>
                        </div>
                        
                        <div className="form-row">
                            <div className="category-label">
                                <label>Passport/ID Card/Driving License</label>
                            </div>
                        </div>
                        <div className="card card-border-top">
                            <div className="card-body">
                                <div className="form-row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="col-form-label">Passport Number</label>
                                            <input name="passportNumber" className="form-control"
                                                placeholder="Passport Number" value={this.state.passportNumber} onChange={this.handleChange} />
                                        </div>
                                    </div>
                                </div>

                                <div className="form-row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="col-form-label">Passport Issue:</label>
                                            <DatePicker className="form-control digits" name="passportIssue" 
                                                selected={this.state.passportIssue} onChange={this.passportIssueChange} />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="col-form-label">Passport Expiry:</label>
                                            <DatePicker name="passportExpiry" className="form-control digits" 
                                                selected={this.state.passportExpiry} onChange={this.passportExpiryChange} />
                                        </div>
                                    </div>
                                </div>

                                <div className="form-row">
                                    <div className="col-md-4">
                                        <div className="form-group">
                                            <label className="col-form-label">ID Card Number</label>
                                            <input name="idCardNumber" className="form-control"
                                                placeholder="ID Card Number" value={this.state.idCardNumber} onChange={this.handleChange} />
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="form-group">
                                            <label className="col-form-label">Driving License Number</label>
                                            <input name="drivingLicenceNo" className="form-control"
                                                placeholder="Driving License Number" value={this.state.drivingLicenceNo} onChange={this.handleChange} />
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="form-group">
                                            <label className="col-form-label">ID Card Expiry</label>
                                            <DatePicker name="idCardExpiry" className="form-control digits" 
                                                selected={this.state.idCardExpiry} onChange={this.idCardExpiryChange} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="form-row">
                            <div className="category-label">
                                <label>CAS</label>
                            </div>
                        </div>
                        <div className="card card-border-top">
                            <div className="card-body">
                                <div className="form-row">
                                    <div className="col-md-4">
                                        <div className="form-group">
                                            <label className="col-form-label">CAS Obtained</label>
                                            <select className="form-control" name="casObtained" 
                                                value={this.state.casObtained} onChange={this.handleChange} >
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="form-group">
                                            <label className="col-form-label">CAS Code</label>
                                            <input name="casCode" className="form-control"
                                                placeholder="CAS Code" value={this.state.casCode} onChange={this.handleChange} />
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="form-group">
                                            <label className="col-form-label">CAS Issued</label>
                                            <select className="form-control" name="casIssued" 
                                                value={this.state.casIssued} onChange={this.handleChange} >
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div className="form-row">
                                    <div className="col-md-4">
                                        <div className="form-group">
                                            <label className="col-form-label">Date Assigned</label>
                                            <DatePicker name="dateAssigned" className="form-control digits" 
                                                selected={this.state.dateAssigned} onChange={this.dateAssignedChange} />
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="form-group">
                                            <label className="col-form-label">Expiry Date</label>
                                            <DatePicker name="expiryDate" className="form-control digits" 
                                                selected={this.state.expiryDate} onChange={this.expiryDateChange} />
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="form-group">
                                            <label className="col-form-label">CAS Withdrawn</label>
                                            <select className="form-control" name="casWithdrawn" 
                                                value={this.state.casWithdrawn} onChange={this.handleChange} >
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div className="form-row">
                                    <div className="col-md-1 mr-3">
                                        <Button variant="primary" onClick={this.handleSave}>Save </Button>
                                    </div>                          
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </Fragment>
        )
    }
}

export default Visa;