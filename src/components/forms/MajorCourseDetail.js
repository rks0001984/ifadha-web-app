import React, {Component, Fragment} from 'react';
import { Button } from 'reactstrap';
import axios from 'axios';
import SweetAlert from 'react-bootstrap-sweetalert';
import "react-datepicker/dist/react-datepicker.css";

class MajorCourseDetail extends Component {

    constructor(){
        super();
        this.state = {
            showAlert: false,
            basicAlertTitle:'',
            basicAlertType:"default",
            eligibility: '',
            englishLevel: '',
            mathLevel: '',
            regEmailAddress: ''
        }
    }

    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        this.setState({[name]: value});
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.selectedUser !== '' && nextProps.selectedUser !== undefined && this.state.regEmailAddress !== nextProps.selectedUser){
            this.setState({
                regEmailAddress: nextProps.selectedUser
            });
            const options = {
                headers: { 'access-token': localStorage.getItem('access_token')}
            };
            axios.get(process.env.REACT_APP_ENDPOINT + `applicant/majorCourseDetails?regEmailAddress=${nextProps.selectedUser}`, options)
            .then(res => {
                const data = res.data;
                this.setState({
                    regEmailAddress: nextProps.selectedUser,
                    eligibility: data.eligibility,
                    englishLevel: data.englishLevel,
                    mathLevel: data.mathLevel
                });
            }).catch(error => {
                console.log(error)
            });
        }
    }

    handleSave = () => { 
        const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };
        axios.post(process.env.REACT_APP_ENDPOINT + 'applicant/majorCourseDetails/create-update', {
            regEmailAddress: this.state.regEmailAddress,
            eligibility: this.state.eligibility,
            englishLevel: this.state.englishLevel,
            mathLevel: this.state.mathLevel
        }, options).then(res => {
            if(res.status === 200) {
                this.setState({
                    showAlert: true,
                    basicAlertTitle: 'Major Course Details save successfully!!!',
                    basicAlertType: "success"
                })
            }
        }).catch(error => {
            this.setState({
                showAlert: true,
                basicAlertTitle: 'Unbale to save Major Course Details!',
                basicAlertType: "error"
            })
        });
    }

    closeAlert = () => {
        this.setState({
            showAlert: false
        });
    }

    render(){
        return (
            <Fragment>
                <SweetAlert
                    show={this.state.showAlert}
                    type={this.state.basicAlertType}
                    title={this.state.basicAlertTitle}
                    onConfirm={this.closeAlert}
                />
                <form className="theme-form theme-from-scroll p-4" id="majorCourseDetailsFrom" >
                    <div className="form-row">
                        <div className="category-label">
                            <label>Student English/Math Levels</label>
                        </div>
                    </div>
                    <div className="card card-border-top">
                        <div className="form-row">
                            <div className="col-md-4">
                                <div className="form-group">
                                    <label className="col-form-label">English Level</label>
                                    <select className="form-control" name="englishLevel" 
                                        value={this.state.englishLevel} onChange={this.handleChange} >
                                        <option value="--Select--">--Select--</option>
                                        <option value="Entry 1">Entry 1</option>
                                        <option value="Entry 2">Entry 2</option>
                                        <option value="Entry 3">Entry 3</option>
                                        <option value="Level 1">Level 1</option>
                                        <option value="Level 2">Level 2</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="form-group">
                                    <label className="col-form-label">Math Level</label>
                                    <select className="form-control" name="mathLevel" 
                                        value={this.state.mathLevel} onChange={this.handleChange} >
                                        <option value="--Select--">--Select--</option>
                                        <option value="Entry 1">Entry 1</option>
                                        <option value="Entry 2">Entry 2</option>
                                        <option value="Entry 3">Entry 3</option>
                                        <option value="Level 1">Level 1</option>
                                        <option value="Level 2">Level 2</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="form-group">
                                    <label className="col-form-label">Eligibility</label>
                                    <select className="form-control" name="eligibility" 
                                        value={this.state.eligibility} onChange={this.handleChange} >
                                        <option value="--Select--">--Select--</option>
                                        <option value="Qualification">Qualification</option>
                                        <option value="Experience">Experience</option>
                                        <option value="Doest Not Meet Criteria">Doest Not Meet Criteria</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="col-md-1 mr-3">
                                <Button variant="primary" onClick={this.handleSave}>Save </Button>
                            </div>                             
                        </div>
                    </div>
                </form>
            </Fragment>
        )
    }
}

export default MajorCourseDetail;