import React, {Component, Fragment} from 'react';
import { Button } from 'reactstrap';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import EducationalBackgroud from './EducationalBackgroud';

class EducationAndExperience extends Component {

    constructor(props){
        super(props);
        this.state = {
             activeTab: '1'          
        }
    }

    setActiveTab = (activeTabName) =>{
        this.setState({
            activeTab: activeTabName
        })
    }

    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        this.setState({[name]: value});
    }

    handleSave = () => {
        //console.log(this.state)
    }

    render(){

        return (
            <Fragment>
                <form className="theme-form theme-from-scroll" id="educationAndExperienceForm" >
                    <div className="card">
                        <div className="card-body">
                            <div className="form-row">
                                <div className="col-sm-12">
                                    <Nav tabs className="border-tab-primary">
                                        <NavItem className="nav nav-tabs" id="myTab" role="tablist">
                                            <NavLink className={this.state.activeTab === '1' ? 'active' : ''} onClick={() => this.setActiveTab('1')}>
                                                Education Background
                                            </NavLink>
                                        </NavItem>
                                        <NavItem className="nav nav-tabs" id="myTab" role="tablist">
                                            <NavLink className={this.state.activeTab === '2' ? 'active' : ''} onClick={() => this.setActiveTab('2')}>
                                                Education Reference
                                            </NavLink>
                                        </NavItem>
                                        <NavItem className="nav nav-tabs" id="myTab" role="tablist">
                                            <NavLink className={this.state.activeTab === '3' ? 'active' : ''} onClick={() => this.setActiveTab('3')}>
                                                Work Experience
                                            </NavLink>
                                        </NavItem>
                                    </Nav>
                                    <TabContent activeTab={this.state.activeTab}>
                                        <TabPane tabId="1">
                                            <form className="theme-form theme-from-scroll p-4" id="eduAndExpForm" >
                                                <div className="form-row">
                                                    <div className="category-label">
                                                        <label>Educational Backgroud</label>
                                                    </div>
                                                </div>
                                                <div className="card card-border-top">
                                                    <EducationalBackgroud />
                                                </div>

                                                <div className="form-row">
                                                    <div className="category-label">
                                                        <label>English Language Proficiency</label>
                                                    </div>
                                                </div>
                                            </form>
                                        </TabPane>
                                        <TabPane tabId="2">
                                            <p className="mb-0 m-t-30">Upcoming History Page</p>
                                        </TabPane>
                                        <TabPane tabId="3">
                                            <p className="mb-0 m-t-30">Upcoming History Page</p>
                                        </TabPane>
                                    </TabContent>
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="col-md-1 mr-3">
                                    <Button variant="primary" onClick={this.handleSave}>Save </Button>
                                </div>                              
                            </div>
                        </div>
                    </div>
                </form>
            </Fragment>
        )
    }
}

export default EducationAndExperience;