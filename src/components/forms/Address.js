import React, {Component, Fragment} from 'react';
import { Button } from 'reactstrap';
import axios from 'axios';
import SweetAlert from 'react-bootstrap-sweetalert';

class Address extends Component {

    constructor(props){
        super(props);
        this.state = {
            appliedCountryAddrCity: "",
            appliedCountryAddrCountry: "",
            appliedCountryAddrLineOne: "",
            appliedCountryAddrLineTwo: "",
            appliedCountryAddrPostCode: "",
            countryApplyFor: "",
            overseasAddrCity: "",
            overseasAddrCountry: "",
            overseasAddrLineOne: "",
            overseasAddrLineTwo: "",
            overseasAddrPostCode: "",
            permanentAddrCity: "",
            permanentAddrCountry: "",
            permanentAddrLineOne: "",
            permanentAddrLineTwo: "",
            permanentAddrPostCode: "",
            showAlert: false,
            basicAlertTitle:'',
            basicAlertType:"default",
            regEmailAddress:''
        }
    }

    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        this.setState({[name]: value});
    }

    handleSave = () => {
        //console.log(this.state)
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.selectedUser !== '' && nextProps.selectedUser !== undefined && this.state.regEmailAddress !== nextProps.selectedUser){
            this.setState({
                regEmailAddress: nextProps.selectedUser
            });
            const options = {
                headers: { 'access-token': localStorage.getItem('access_token')}
            };
            axios.get(process.env.REACT_APP_ENDPOINT + `applicant/address?regEmailAddress=${nextProps.selectedUser}`, options)
            .then(res => {
                const data = res.data;
                this.setState({
                    regEmailAddress: nextProps.selectedUser,
                    appliedCountryAddrCity: data.appliedCountryAddrCity,
                    appliedCountryAddrCountry: data.appliedCountryAddrCountry,
                    appliedCountryAddrLineOne: data.appliedCountryAddrLineOne,
                    appliedCountryAddrLineTwo: data.appliedCountryAddrLineTwo,
                    appliedCountryAddrPostCode: data.appliedCountryAddrPostCode,
                    countryApplyFor: data.countryApplyFor,
                    overseasAddrCity: data.overseasAddrCity,
                    overseasAddrCountry: data.overseasAddrCountry,
                    overseasAddrLineOne: data.overseasAddrLineOne,
                    overseasAddrLineTwo: data.overseasAddrLineTwo,
                    overseasAddrPostCode: data.overseasAddrPostCode,
                    permanentAddrCity: data.permanentAddrCity,
                    permanentAddrCountry: data.permanentAddrCountry,
                    permanentAddrLineOne: data.permanentAddrLineOne,
                    permanentAddrLineTwo: data.permanentAddrLineTwo,
                    permanentAddrPostCode: data.permanentAddrPostCode
                });
            }).catch(error => {
                console.log(error)
            });
        }
    }

    handleSave = () => { 
        const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };
        axios.post(process.env.REACT_APP_ENDPOINT + 'applicant/address/create-update', {
            regEmailAddress: this.state.regEmailAddress,
            appliedCountryAddrCity: this.state.appliedCountryAddrCity,
            appliedCountryAddrCountry: this.state.appliedCountryAddrCountry,
            appliedCountryAddrLineOne: this.state.appliedCountryAddrLineOne,
            appliedCountryAddrLineTwo: this.state.appliedCountryAddrLineTwo,
            appliedCountryAddrPostCode: this.state.appliedCountryAddrPostCode,
            countryApplyFor: this.state.countryApplyFor,
            overseasAddrCity: this.state.overseasAddrCity,
            overseasAddrCountry: this.state.overseasAddrCountry,
            overseasAddrLineOne: this.state.overseasAddrLineOne,
            overseasAddrLineTwo: this.state.overseasAddrLineTwo,
            overseasAddrPostCode: this.state.overseasAddrPostCode,
            permanentAddrCity: this.state.permanentAddrCity,
            permanentAddrCountry: this.state.permanentAddrCountry,
            permanentAddrLineOne: this.state.permanentAddrLineOne,
            permanentAddrLineTwo: this.state.permanentAddrLineTwo,
            permanentAddrPostCode: this.state.permanentAddrPostCode
        }, options).then(res => {
            if(res.status === 200) {
                this.setState({
                    showAlert: true,
                    basicAlertTitle: 'Address save successfully!!!',
                    basicAlertType: "success"
                })
            }
        }).catch(error => {
            this.setState({
                showAlert: true,
                basicAlertTitle: 'Unbale to save Address!',
                basicAlertType: "error"
            })
        });
    }

    closeAlert = () => {
        this.setState({
            showAlert: false
        });
    }

    render(){
        return (
            <Fragment>
                <SweetAlert
                    show={this.state.showAlert}
                    type={this.state.basicAlertType}
                    title={this.state.basicAlertTitle}
                    onConfirm={this.closeAlert}
                />
                <form className="theme-form theme-from-scroll p-4" id="addressForm" >
                    <div className="form-row">
                        <div className="category-label">
                            <label>Permanent Address</label>
                        </div>
                    </div>
                    <div className="card card-border-top">
                        <div className="card-body">
                            <div className="form-row">
                                <div className="col-md-12">
                                    <div className="form-group">
                                        <label className="col-form-label">Address Line 1:</label>
                                        <input name="appliedCountryAddrLineOne" className="form-control"
                                            placeholder="Address" value={this.state.appliedCountryAddrLineOne} onChange={this.handleChange} />
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-12">
                                    <div className="form-group">
                                        <label className="col-form-label">Address Line 2:</label>
                                        <input name="appliedCountryAddrLineTwo" className="form-control"
                                            placeholder="Address" value={this.state.appliedCountryAddrLineTwo} onChange={this.handleChange} />
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">City</label>
                                        <input name="appliedCountryAddrCity" className="form-control"
                                            placeholder="City" value={this.state.appliedCountryAddrCity} onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Postcode</label>
                                        <input name="appliedCountryAddrPostCode" className="form-control"
                                            placeholder="Postcode" value={this.state.appliedCountryAddrPostCode} onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Country</label>
                                        <input name="appliedCountryAddrCountry" className="form-control"
                                            placeholder="Country" value={this.state.appliedCountryAddrCountry} onChange={this.handleChange} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div className="form-row">
                        <div className="category-label">
                            <label>UK Address (If Any)</label>
                        </div>
                    </div>
                    <div className="card card-border-top">
                        <div className="card-body">
                            <div className="form-row">
                                <div className="col-md-12">
                                    <div className="form-group">
                                        <label className="col-form-label">Address Line 1:</label>
                                        <input name="permanentAddrLineOne" className="form-control"
                                            placeholder="Address" value={this.state.permanentAddrLineOne} onChange={this.handleChange} />
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-12">
                                    <div className="form-group">
                                        <label className="col-form-label">Address Line 2:</label>
                                        <input name="permanentAddrLineTwo" className="form-control"
                                            placeholder="Address" value={this.state.permanentAddrLineTwo} onChange={this.handleChange} />
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">City</label>
                                        <input name="permanentAddrCity" className="form-control"
                                            placeholder="City" value={this.state.permanentAddrCity} onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Postcode</label>
                                        <input name="permanentAddrPostCode" className="form-control"
                                            placeholder="Postcode" value={this.state.permanentAddrPostCode} onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Country</label>
                                        <input name="permanentAddrCountry" className="form-control"
                                            placeholder="Country" value={this.state.permanentAddrCountry} onChange={this.handleChange} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="form-row">
                        <div className="category-label">
                            <label>Overseas Address</label>
                        </div>
                    </div>
                    <div className="card card-border-top">
                        <div className="card-body">
                            <div className="form-row">
                                <div className="col-md-12">
                                    <div className="form-group">
                                        <label className="col-form-label">Address Line 1:</label>
                                        <input name="overseasAddrLineOne" className="form-control"
                                            placeholder="Address" value={this.state.overseasAddrLineOne} onChange={this.handleChange} />
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-12">
                                    <div className="form-group">
                                        <label className="col-form-label">Address Line 2:</label>
                                        <input name="overseasAddrLineTwo" className="form-control"
                                            placeholder="Address" value={this.state.overseasAddrLineTwo} onChange={this.handleChange} />
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">City</label>
                                        <input name="overseasAddrCity" className="form-control"
                                            placeholder="City" value={this.state.overseasAddrCity} onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Postcode</label>
                                        <input name="overseasAddrPostCode" className="form-control"
                                            placeholder="Postcode" value={this.state.overseasAddrPostCode} onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label className="col-form-label">Country</label>
                                        <input name="overseasAddrCountry" className="form-control"
                                            placeholder="Country" value={this.state.overseasAddrCountry} onChange={this.handleChange} />
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-1 mr-3">
                                    <Button variant="primary" onClick={this.handleSave}>Save </Button>
                                </div>                            
                            </div>

                        </div>
                    </div>

                </form>
            </Fragment>
        )
    }
}

export default Address;