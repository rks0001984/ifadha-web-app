import React, {Component, Fragment} from 'react';
import { Button } from 'reactstrap';
import axios from 'axios';
import SweetAlert from 'react-bootstrap-sweetalert';
import "react-datepicker/dist/react-datepicker.css";

class MarketingConsultant extends Component {

    constructor(props){
        super(props);
        this.state = {
            regEmailAddress: '',
            marketingConsutant: '',
            showAlert: false,
            basicAlertTitle:'',
            basicAlertType:"default"
        }
    }

    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        this.setState({[name]: value});
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.selectedUser !== '' && nextProps.selectedUser !== undefined && this.state.regEmailAddress !== nextProps.selectedUser){
            this.setState({
                regEmailAddress: nextProps.selectedUser
            });
            const options = {
                headers: { 'access-token': localStorage.getItem('access_token')}
            };
            axios.get(process.env.REACT_APP_ENDPOINT + `applicant/marketingConsultant?regEmailAddress=${nextProps.selectedUser}`, options)
            .then(res => {
                const data = res.data;
                this.setState({
                    regEmailAddress: nextProps.selectedUser,
                    marketingConsutant: data.marketingConsutant
                });
            }).catch(error => {
                console.log(error)
            });
        }
    }

    handleSave = () => { 
        const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };
        axios.post(process.env.REACT_APP_ENDPOINT + 'applicant/marketingConsultant/create-update', {
            regEmailAddress: this.state.regEmailAddress,
            marketingConsutant: this.state.marketingConsutant
        }, options).then(res => {
            if(res.status === 200) {
                this.setState({
                    showAlert: true,
                    basicAlertTitle: 'Marketing Consultant save successfully!!!',
                    basicAlertType: "success"
                })
            }
        }).catch(error => {
            this.setState({
                showAlert: true,
                basicAlertTitle: 'Unbale to save Marketing Consultant!',
                basicAlertType: "error"
            })
        });
    }

    closeAlert = () => {
        this.setState({
            showAlert: false
        });
    }

    render(){
        return (
            <Fragment>
                <SweetAlert
                    show={this.state.showAlert}
                    type={this.state.basicAlertType}
                    title={this.state.basicAlertTitle}
                    onConfirm={this.closeAlert}
                />
                <form className="theme-form theme-from-scroll p-4" id="marketingConultantForm" >
                    <div className="form-row">
                        <div className="category-label">
                            <label>Marketing Consultant</label>
                        </div>
                    </div>
                    <div className="card card-border-top">
                        <div className="form-row">
                            <div className="col-md-12">
                                <div className="form-group p-4">
                                    <select className="form-control" name="marketingConsutant" 
                                        value={this.state.marketingConsutant} onChange={this.handleChange} >
                                        <option value="default">------------- Select Marketing Consultant ---------------</option>
                                        <option value="Consutant Option 1">Consutant Option 1</option>
                                        <option value="Consutant Option 2">Consutant Option 2</option>
                                        <option value="Consutant Option 3">Consutant Option 3</option>
                                        <option value="Consutant Option 4">Consutant Option 4</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="col-md-1 mr-3">
                                <Button variant="primary" onClick={this.handleSave}>Save </Button>
                            </div>                             
                        </div>
                    </div>
                </form>
            </Fragment>
        )
    }
}

export default MarketingConsultant;