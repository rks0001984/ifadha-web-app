import React, { Component } from "react";
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

class TableModel extends Component {
  constructor(props) {
      super(props);
      this.state = {
          sortName: undefined,
          sortOrder: undefined
      };

      this.options = {
          onSortChange: this.onSortChange.bind(this),            
          onPageChange: this.onPageChange.bind(this),
          onSizePerPageList: this.sizePerPageListChange.bind(this),
          sortIndicator: true
      };
      this.onRowSelect = this.onRowSelect.bind(this);
      this.onSelectAll = this.onSelectAll.bind(this);
  }

  sizePerPageListChange(sizePerPage) {
      //alert(`sizePerPage: ${sizePerPage}`);
  }

  onPageChange(page, sizePerPage) {
      //alert(`page: ${page}, sizePerPage: ${sizePerPage}`);
  }

  onSortChange(sortName, sortOrder) {
      this.setState({
          sortName,
          sortOrder
      });
  }

  onRowSelect = (row, isSelected, e) => {
    const{ addSelectedRowObj } = this.props;
    const{ removeUnSelectedRowObj } = this.props;   
    if (isSelected) {
      addSelectedRowObj(row);
    } else {
      removeUnSelectedRowObj(row);
    }
  }

  onSelectAll = (isSelected, rows) => {    
    const{ addSelectedRowObj } = this.props;
    const{ removeUnSelectedRowObj } = this.props; 
    if (isSelected) {
      addSelectedRowObj(rows);
    } else {
      removeUnSelectedRowObj(rows);
    }
  }

  render() {
      const selectRowProp = {
          mode: 'checkbox',
          clickToSelect: true,
          bgColor: '#e9ecef',            
          onSelect: this.onRowSelect,
          onSelectAll: this.onSelectAll
      };

      const tableRows = this.props.columns.map((column) => (
          <TableHeaderColumn headerAlign='left' dataAlign='left' dataField={column.accessor} dataSort>
            {column.Header}
          </TableHeaderColumn>
      ));

      return (
            <BootstrapTable pagination  striped hover condensed
                    height='320' scrollTop={ 'Bottom' }  options={ this.options } selectRow={ selectRowProp }
                    data={this.props.data}  >
                <TableHeaderColumn headerAlign='left' hidden dataAlign='left' isKey dataField='id' dataSort>
                  Unique ID
                </TableHeaderColumn>
                {tableRows}
            </BootstrapTable>
      );
  }
}

export default TableModel;