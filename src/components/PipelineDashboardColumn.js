import React, {Component} from 'react';
import PipelineDashboardCard from "./PipelineDashboardCard";

class PipelineDashboardColumn extends Component {
	constructor(props) {
		super(props);
		this.state = ({ mouseIsHovering: false });
	}

	componentWillReceiveProps(nextProps) {
		this.state = ({ mouseIsHovering: false });
	}

	componentWillMount() {
		
	}

	generateKanbanCards() {
		return this.props.projects.slice(0).map((project) => {
			return (
				<PipelineDashboardCard
					project={project}
					key={project.name}
					onDragEnd={this.props.onDragEnd}
				/>
			);
		});
	}

	render() {
		const columnStyle = {
			'display': 'inline-block',
			'verticalAlign': 'top',
			'marginRight': '5px',
			'marginBottom': '5px',
			'paddingLeft': '5px',
			'paddingTop': '0px',
			'width': '24.6%',
			'textAlign': 'center',
			'backgroundColor': (this.state.mouseIsHovering) ? '#d3d3d3' : '#f0eeee',
		};
		return  (
            <div
				style={columnStyle}
				onDragEnter={(e) => {this.setState({ mouseIsHovering: true }); this.props.onDragEnter(e, this.props.stage);}}
				onDragExit={(e) => {this.setState({ mouseIsHovering: false });}}
			>
				<h4><b>{this.props.name} ({this.props.projects.length})</b></h4>
				{this.generateKanbanCards()}
				<br/>
      </div>);
	}
}

export default PipelineDashboardColumn;