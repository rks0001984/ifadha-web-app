import React, { Fragment, Component } from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import ifadhaLogo from '../assets/images/logo/ifadha-logo.png';

class Login extends Component {
    constructor(props){
        super(props);
        localStorage.setItem('access_token', "");
        localStorage.setItem('access_id', "");
        this.state = ({
            emailAddress: '',
            password: '',
            loginError:'',
            user:{}
        });
    }

    handleLogin = (data) => {
        const selectedUser = this.state.emailAddress;
        const options = {
            headers: { 'access-token': data}
        };
        axios.get(process.env.REACT_APP_ENDPOINT + `usermgm/user/${selectedUser}`, options)
        .then(response => {
            if(response.status === 200){
                localStorage.setItem('access_role', response.data.roles);
                localStorage.setItem('access_fullname', response.data.firstName + " " + response.data.lastName);
                localStorage.setItem('access_token', data);
                localStorage.setItem('access_id', this.state.emailAddress);
                localStorage.setItem('profileImageUri', response.data.profileImageUri);

                const { isAuthenticated } = this.props;
                isAuthenticated(true);

                this.props.history.push("/home/dashboard");
            }
        }).catch(error => {
            console.log(error)
        });
    }

    handleSubmit = event => {
        event.preventDefault();
        const options = {
            headers: { 'Authorization':'Basic ' + btoa(this.state.emailAddress+':'+this.state.password)}
        };
        axios.get(process.env.REACT_APP_ENDPOINT +`index/login`, 
            options).then(response => {
                if(response.status === 200){
                    this.handleLogin(response.data);
                }                
            }).catch(error => {
                this.setState({
                    loginError: error.response != undefined ? error.response.data.message : ''
                })
            })     
    }

    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        this.setState({
            [name]: value
        });
    }

    render(){
        return (
            <Fragment>
                <div className="page-wrapper">
                    <div className="auth-bg">
                        <div className="authentication-box">
                            <div className="text-center">
                                <img src={ifadhaLogo} alt="" />
                                <span className="custtitle">IFADHA</span>
                            </div>
                            <div className="card mt-4">
                                <div className="card-body">
                                    <div className="text-center">
                                        <h4>LOGIN</h4>
                                        <h6>Enter your Email and Password </h6>
                                    </div>
                                    <form className="theme-form" id="loginForm" onSubmit={this.handleSubmit}>
                                        <div className="form-group">
                                            <label className="col-form-label pt-0">Email</label>
                                            <input className="form-control" name="emailAddress" type="text"
                                                 required={true} onChange={this.handleChange} />
                                        </div>
                                        <div className="form-group">
                                            <label className="col-form-label">Password</label>
                                            <input className="form-control" name="password" type="password" 
                                                required={true} onChange={this.handleChange} />
                                        </div>
                                        <div className="form-group form-row mt-3 mb-0">
                                            <button className="btn btn-primary btn-block" type="submit">Login</button>
                                        </div>                                        
                                        <div className="form-group">
                                            <label className="srv-validation-message">{this.state.loginError}</label>
                                        </div>
                                        <div className="col-sm-8">
                                            <div className="text-left mt-2 m-l-20">Forgot password?  
                                                <Link to={`${process.env.PUBLIC_URL}/`}>Regenerate</Link>
                                            </div>
                                        </div>
                                        <div className="col-sm-10">
                                            <div className="text-left mt-2 m-l-20">Not registered student?  
                                                <Link to={`${process.env.PUBLIC_URL}/signUpStudent`}>Signup Student</Link>
                                            </div>
                                        </div>
                                        <div className="col-sm-10">
                                            <div className="text-left mt-2 m-l-20">Not registered admin?  
                                                <Link to={`${process.env.PUBLIC_URL}/signUpAdmin`}>Signup Admin</Link>
                                            </div>
                                        </div>
                                        <div className="login-divider"></div>
                                        <div className="social mt-3">
                                            <div className="form-group btn-showcase d-flex">
                                                <button className="btn social-btn btn-twitter d-inline-block">
                                                    <i className="fa fa-google"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default Login;