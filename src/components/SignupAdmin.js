import React, { Fragment, Component } from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import ifadhaLogo from '../assets/images/logo/ifadha-logo.png';

const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);

class SignupAdmin extends Component {
    constructor(props){
        super(props);
        this.state = ({
            firstName: '',
            lastName: '',
            emailAddress: '',
            password: '',
            mobileNumber: '',
            middleName:'',
            role: 'SUPER_ADMIN',
            username: '',
            orgName: '',
            errors: {
                firstName: '', lastName: '', emailAddress: '', mobileNumber: '', username: ''
            },
            userCreationError:''
        });
    }

    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        let errors = this.state.errors;
    
        switch (name) {
            case 'firstName': 
                errors.firstName = 
                    value.length < 4
                    ? 'First Name must be 4 characters long!'
                    : '';
                break;
            case 'emailAddress': 
                errors.emailAddress = 
                validEmailRegex.test(value)
                    ? ''
                    : 'Email Address is not valid!';
                break;
            case 'mobileNumber': 
                errors.mobileNumber = 
                value.length !== 10
                    ? 'Mobile number must be 10 characters long!'
                    : '';
                break;
            default:
                break;
        }
        this.setState({errors, [name]: value});
    }

    successfulAuth = (data) => {
        this.props.history.push("/");
    }

    handleSubmit = event => {
        event.preventDefault();
        if (this.state.firstName.length == 0 || 
            this.state.lastName.length == 0 ||
            this.state.emailAddress.length == 0 ||
            this.state.mobileNumber.length == 0 ||
            this.state.errors.firstName.length > 0 ||
            this.state.errors.lastName.length > 0 ||
            this.state.errors.emailAddress.length > 0 || 
            this.state.errors.mobileNumber.length > 0) {
            return;
        } else {
            axios.post(process.env.REACT_APP_ENDPOINT + `index/signup`, { 
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                mobileNumber: this.state.mobileNumber,
                password: this.state.password,
                emailAddress: this.state.emailAddress,
                middleName: this.state.middleName,
                role: this.state.role,
                orgName: this.state.orgName
            }).then(response => {
                if(response.status == 200){
                    this.successfulAuth(response.data);
                }
            }).catch(error => {
                this.setState({
                    userCreationError: error.response != "undefined" ? error.response.data.message : "Unable to signup"
                })
            })
        }      
    }

    render(){
        return (
            <Fragment>
                <div className="page-wrapper">
                    <div className="auth-bg">
                        <div className="authentication-box">
                            <div className="text-center">
                                <img src={ifadhaLogo} alt="" />
                                <span className="custtitle">IFADHA</span>
                            </div>
                            <div className="card mt-4 p-4">
                                <h4 className="text-center">Admin Registration Form</h4>
                                <h6 className="text-center">Password will be sent to provided email address.</h6>
                                <form className="theme-form" id="signUpForm" onSubmit={this.handleSubmit}>
                                    <div className="form-row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label className="col-form-label">First Name</label>
                                                <input name="firstName" value={this.state.firstName} className="form-control" type="text" 
                                                    placeholder="Your First Name" onChange={this.handleChange} />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label className="col-form-label">Last Name</label>
                                                <input name="lastName" value={this.state.lastName} className="form-control" type="text"
                                                     placeholder="Your Last Name" onChange={this.handleChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label className="col-form-label">Email</label>
                                        <input name="emailAddress" className="form-control" type="text"  value={this.state.emailAddress}
                                            placeholder="Your Email Address" onChange={this.handleChange} />
                                    </div>
                                    <div className="form-group">
                                        <label className="col-form-label">Mobile Number</label>
                                        <input name="mobileNumber" className="form-control" type="text"  value={this.state.mobileNumber}
                                            placeholder="Your Mobile Number" onChange={this.handleChange} />
                                    </div>                                    
                                    <div className="form-group">
                                        <label className="col-form-label">Organization Name</label>
                                        <input name="orgName" className="form-control" type="text"  value={this.state.orgName}
                                            placeholder="Organization Name" onChange={this.handleChange} />
                                    </div>
                                    <div className="form-row">
                                        <div className="col-sm-4">
                                            <button className="btn btn-primary" type="submit">Sign Up</button>
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="col-sm-8">
                                            <div className="text-left mt-2 m-l-20">Are you already user?  
                                                <Link to={`${process.env.PUBLIC_URL}/`}>Login</Link>
                                            </div>
                                        </div>
                                        <div className="col-sm-8">
                                            <div className="text-left mt-2 m-l-20">Signup as Student?  
                                                <Link to={`${process.env.PUBLIC_URL}/signUpStudent`}>Signup Student</Link>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label className="srv-validation-message">{this.state.userCreationError}</label>
                                        <label className="srv-validation-message">{this.state.errors.firstName}</label>
                                        <label className="srv-validation-message">{this.state.errors.lastName}</label>
                                        <label className="srv-validation-message">{this.state.errors.emailAddress}</label>
                                        <label className="srv-validation-message">{this.state.errors.mobileNumber}</label>
                                    </div>
                                    <div className="form-divider"></div>
                                    <div className="social mt-3">
                                        <div className="form-group btn-showcase d-flex">
                                            <button className="btn social-btn btn-twitter d-inline-block">
                                                <i className="fa fa-google"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default SignupAdmin;