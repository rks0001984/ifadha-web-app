import React, { Component, Fragment } from 'react';
import axios from 'axios';
import { Typeahead } from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { TabContent, TabPane, Nav, NavItem, NavLink, Button } from 'reactstrap';
import PersonalCourseDetails from './forms/PersonalCourseDetails';
import Address from './forms/Address';
import Visa from './forms/Visa';
import MajorCourseDetail from './forms/MajorCourseDetail';
import MarketingConsultant from './forms/MarketingConsultant';
import Notes from './forms/Notes';
import ReferenceDetails from './forms/ReferenceDetails';
import Attachments from './forms/Attachments';
import EducationAndExperience from './forms/EducationAndExperience';
import SweetAlert from 'react-bootstrap-sweetalert';
import { Column, Row } from 'simple-flexbox';
import ReactDOMServer from "react-dom/server";
import jsPDF from 'jspdf';

class ApplicantForm extends Component{

    constructor(props){
        super(props);
        
        this.state = {
            userEmails: [],
            fullNames: [],
            selectedUser: this.props.emailToNavigate !== '' ? this.props.emailToNavigate : '',
            selectedUserRole: localStorage.getItem('access_role'),
            isStudent: localStorage.getItem('access_role') === 'STUDENT',
            isAdmin: localStorage.getItem('access_role') === 'SUPER_ADMIN' || localStorage.getItem('access_role') === 'ADMIN',
            isAgent: localStorage.getItem('access_role') === 'STAFF' || localStorage.getItem('access_role') === 'AGENT',
            referenceNumber: '',
            activeParentTab: 'appFormTab',
            activeChildTab: 'refrenceDetailsTab',
            showAlert: false,
            basicAlertTitle:'',
            basicAlertType:"default"
        }
    }

    closeAlert = () => {
        this.setState({
            showAlert: false
        });
    }

    submitForm = () => {
        const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };

        axios.get(process.env.REACT_APP_ENDPOINT + `applicant/submit/${this.state.selectedUser}`, options)
        .then(response => {
            if(response.status === 200) {
                this.setState({
                    collapseEditProfile:false,
                    showAlert: true,
                    basicAlertTitle: ' Application form updated!',
                    basicAlertType: "success"
               });
            }
        }).catch(error => {
            this.setState({
                basicAlertTitle: ' Application Form failed to update!',
                basicAlertType: "error",
                showAlert: true
            });
        });
    }

    componentDidMount() {
        const { setTitle } = this.props;
        setTitle('Application');
        
        const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };
        if(this.state.isStudent){
            this.setState({
                selectedUser: localStorage.getItem('access_id')
            })
        } else {
            axios.get(process.env.REACT_APP_ENDPOINT + `usermgm/user/all/STUDENT?sort=id,desc`, options)
            .then(response => {
                if(response.status === 200){
                    response.data.map(users => {
                        this.setState({ 
                            userEmails: [...this.state.userEmails, users.emailAddress],
                            fullNames: [...this.state.fullNames, users.fullName] 
                        })
                    })
                } else if(response.status === 1015) {
                    
                }
                
            }).catch(error => {
                console.log(error)
            });
        }        
    }

    updateReferenceNumber = refNumber => {
        if(refNumber !== '' && refNumber !== this.state.referenceNumber) {
            this.setState({
                referenceNumber: refNumber
            })
        }
    }

    setActiveChildTab = (activeTabName) =>{
        this.setState({
            activeChildTab: activeTabName
        })
    }

    setActiveParentTab = (activeTabName) =>{
        this.setState({
            activeParentTab: activeTabName,
            setActiveChildTab: activeTabName === 'appFormTab' ? 'refrenceDetailsTab' : 'communicationTab'
        })
    }

    downloadForm = () => {
        const doc = new jsPDF();
        doc.fromHTML(ReactDOMServer.renderToStaticMarkup(this.render()));
        doc.save("myDocument.pdf");
    }

    render() {
        const disableLink = (this.state.referenceNumber === '' || this.state.referenceNumber === null) ? true : false;

        return (
                <Fragment>
                    <SweetAlert
                        show={this.state.showAlert}
                        type={this.state.basicAlertType}
                        title={this.state.basicAlertTitle}
                        onConfirm={this.closeAlert}
                    />
                    <Row className="col-xl-12 applicant-ser-row" hidden={this.state.isStudent}>
                        <Column className="applicant-ser-col-left">
                            <i class="fa fa-user" aria-hidden="true"></i> Applicant 
                        </Column>
                        <Column className="applicant-ser-col-right">
                            <Typeahead
                                className="typea-head-dropdown"
                                id="multiple-typeahead"
                                clearButton={true}
                                onChange={(selectedUser) => {
                                    if(selectedUser[0] !== undefined){
                                        this.setState({
                                            selectedUser: selectedUser[0].slice(selectedUser[0].indexOf('(')+1, selectedUser[0].length-1),
                                        });
                                    } else {
                                        this.setState({
                                            selectedUser: '',
                                        });
                                    }
                                }}
                                labelKey="emailAddress"
                                options={this.state.fullNames}
                                defaultInputValue={this.state.selectedUser}
                                placeholder="Search here..."
                            />
                        </Column>
                    </Row>

                    <div className="col-sm-12 appliant-tabs">
                        <Nav tabs className="border-tab-primary">
                            <NavItem className="nav nav-tabs"  id="appFormTab" role="tablist">
                                <NavLink className={this.state.activeParentTab === 'appFormTab' ? 'active' : ''} onClick={() => this.setActiveParentTab('appFormTab')}>
                                    Application
                                </NavLink>
                            </NavItem>
                            <NavItem className="nav nav-tabs" id="communicationTab" role="tablist">
                                <NavLink className={this.state.activeParentTab === 'communicationTab' ? 'active' : ''} onClick={() => this.setActiveParentTab('communicationTab')}>
                                    Communication
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.activeParentTab} className="appliant-tabs-content">
                            <TabPane tabId="appFormTab">
                                <div hidden={this.state.selectedUser === ''} className="col-xl-12 p-t-5">
                                    <Nav tabs className="border-tab-primary">
                                        <NavItem className="nav nav-tabs"  id="refrenceDetailsTab" role="tablist">
                                            <NavLink className={this.state.activeChildTab === 'refrenceDetailsTab' ? 'active' : ''} onClick={() => this.setActiveChildTab('refrenceDetailsTab')}>
                                                Reference Details
                                            </NavLink>
                                        </NavItem>
                                        <NavItem className="nav nav-tabs" id="personalCourseDetailsTab" role="tablist">
                                            <NavLink disabled={disableLink} className={this.state.activeChildTab === 'personalCourseDetailsTab' ? 'active' : ''} onClick={() => this.setActiveChildTab('personalCourseDetailsTab')}>
                                                Personal/Course Details
                                            </NavLink>
                                        </NavItem>
                                        <NavItem  className="nav nav-tabs"  id="addressTab" role="tablist">
                                            <NavLink disabled={disableLink} className={this.state.activeChildTab === 'addressTab' ? 'active' : ''} onClick={() => this.setActiveChildTab('addressTab')}>
                                                Address
                                            </NavLink>
                                        </NavItem>
                                        <NavItem className="nav nav-tabs" id="visaDetailsTab" role="tablist">
                                            <NavLink disabled={disableLink} className={this.state.activeChildTab === 'visaDetailsTab' ? 'active' : ''} onClick={() => this.setActiveChildTab('visaDetailsTab')}>
                                                Visa Details
                                            </NavLink>
                                        </NavItem>
                                        <NavItem className="nav nav-tabs"  id="educationExperienceTab" role="tablist">
                                            <NavLink disabled={disableLink} className={this.state.activeChildTab === 'educationExperienceTab' ? 'active' : ''} onClick={() => this.setActiveChildTab('educationExperienceTab')}>
                                                Education/Experience
                                            </NavLink>
                                        </NavItem>
                                        <NavItem className="nav nav-tabs" id="attachmentsTab" role="tablist">
                                            <NavLink disabled={disableLink} className={this.state.activeChildTab === 'attachmentsTab' ? 'active' : ''} onClick={() => this.setActiveChildTab('attachmentsTab')}>
                                                Attachments
                                            </NavLink>
                                        </NavItem>
                                        <NavItem className="nav nav-tabs"  id="initialAssessmentTab" role="tablist">
                                            <NavLink disabled={disableLink} className={this.state.activeChildTab === 'initialAssessmentTab' ? 'active' : ''} onClick={() => this.setActiveChildTab('initialAssessmentTab')}>
                                                Initial Assessment
                                            </NavLink>
                                        </NavItem>
                                        <NavItem className="nav nav-tabs" id="majorCourseDetailsTab" role="tablist">
                                            <NavLink disabled={disableLink} className={this.state.activeChildTab === 'majorCourseDetailsTab' ? 'active' : ''} onClick={() => this.setActiveChildTab('majorCourseDetailsTab')}>
                                                Major Course Details
                                            </NavLink>
                                        </NavItem>
                                    </Nav>

                                    <TabContent activeTab={this.state.activeChildTab} className="appliant-tabs-content">
                                        <TabPane tabId="refrenceDetailsTab">
                                            <ReferenceDetails selectedUserRole={this.state.selectedUserRole} selectedUser={this.state.selectedUser}
                                                updateReferenceNumber={this.updateReferenceNumber}/>
                                        </TabPane>
                                        <TabPane tabId="personalCourseDetailsTab">
                                            <PersonalCourseDetails selectedUserRole={this.state.selectedUserRole} selectedUser={this.state.selectedUser}/>
                                        </TabPane>
                                        <TabPane tabId="addressTab">
                                            <Address selectedUserRole={this.state.selectedUserRole} selectedUser={this.state.selectedUser}/>
                                        </TabPane>
                                        <TabPane tabId="visaDetailsTab">
                                            <Visa selectedUserRole={this.state.selectedUserRole} selectedUser={this.state.selectedUser}/>
                                        </TabPane>
                                        <TabPane tabId="educationExperienceTab">
                                            <EducationAndExperience selectedUserRole={this.state.selectedUserRole} selectedUser={this.state.selectedUser}/>
                                        </TabPane>
                                        <TabPane tabId="attachmentsTab">
                                            <Attachments selectedUserRole={this.state.selectedUserRole} selectedUser={this.state.selectedUser}/>
                                        </TabPane>
                                        <TabPane tabId="initialAssessmentTab">
                                            <p>TODO: Add form here...</p>
                                        </TabPane>
                                        <TabPane tabId="majorCourseDetailsTab">
                                            <MajorCourseDetail selectedUserRole={this.state.selectedUserRole} selectedUser={this.state.selectedUser}/>
                                        </TabPane>
                                    </TabContent>
                                </div>
                            </TabPane>
                            <TabPane tabId="communicationTab">
                                <div hidden={this.state.referenceNumber === ''} className="col-xl-12 mt-2">
                                    <Nav tabs className="border-tab-primary">
                                        <NavItem className="nav nav-tabs"  id="communicationTab" role="tablist">
                                            <NavLink className={this.state.activeChildTab === 'communicationTab' ? 'active' : ''} onClick={() => this.setActiveChildTab('communicationTab')}>
                                                Communication
                                            </NavLink>
                                        </NavItem>
                                        <NavItem className="nav nav-tabs" id="notesTab" role="tablist">
                                            <NavLink className={this.state.activeChildTab === 'notesTab' ? 'active' : ''} onClick={() => this.setActiveChildTab('notesTab')}>
                                                Notes
                                            </NavLink>
                                        </NavItem>
                                        <NavItem className="nav nav-tabs"  id="marketingConsultantTab" role="tablist">
                                            <NavLink className={this.state.activeChildTab === 'marketingConsultantTab' ? 'active' : ''} onClick={() => this.setActiveChildTab('marketingConsultantTab')}>
                                                Marketing Consultant
                                            </NavLink>
                                        </NavItem>
                                        <NavItem className="nav nav-tabs" id="logTab" role="tablist">
                                            <NavLink className={this.state.activeChildTab === 'logTab' ? 'active' : ''} onClick={() => this.setActiveChildTab('logTab')}>
                                                Log
                                            </NavLink>
                                        </NavItem>
                                    </Nav>

                                    <TabContent activeTab={this.state.activeChildTab} className="appliant-tabs-content">
                                        <TabPane tabId="communicationTab">
                                            <p>TODO: Add form here...</p>
                                        </TabPane>
                                        <TabPane tabId="notesTab">
                                            <Notes selectedUserRole={this.state.selectedUserRole} selectedUser={this.state.selectedUser}/>
                                        </TabPane>
                                        <TabPane tabId="marketingConsultantTab">
                                            <MarketingConsultant selectedUserRole={this.state.selectedUserRole} selectedUser={this.state.selectedUser}/>
                                        </TabPane>
                                        <TabPane tabId="logTab">
                                            <p>TODO: Add form here...</p>
                                        </TabPane>
                                    </TabContent>
                                </div>
                            </TabPane>
                        </TabContent>
                    </div>
                    
                    <div className="align-items-sm-center" hidden={this.state.referenceNumber === ''}>
                        <div class="text-center">
                            <Button className="btn-primary" onClick={this.submitForm}>Submit Application</Button>
                            <Button className="ml-2 btn-primary" onClick={this.downloadForm}>Print Application</Button>
                        </div>
                    </div>
                 
                </Fragment>
        );
    }
}

export default ApplicantForm;