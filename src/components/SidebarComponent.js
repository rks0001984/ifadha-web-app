import React, {Component, Fragment} from 'react';
import { Row, Column} from 'simple-flexbox';
import ifadhaLogo from '../assets/images/logo/ifadha-logo.png';
import { Home, User, Airplay, Users, Settings, Box } from 'react-feather';
import { Link } from 'react-router-dom';

class SidebarComponent extends Component{
    constructor(props){
        super(props);
        this.state = ({
            collapseUsers: true,
            collapseSettings: true
        })
    }

    toggleUsers = () => {
        this.setState({
            collapseUsers: !this.state.collapseUsers,
            collapseSettings: true
        })
    }

    toggleSettings = () => {
        this.setState({
            collapseSettings: !this.state.collapseSettings,
            collapseUsers: true
        })
    }

    resetToggle = () => {
        this.setState({
            collapseUsers: true,
            collapseSettings: true
        })
    }

    render() {
        const isStudent = localStorage.getItem('access_role') === 'STUDENT';
        const isStaff = localStorage.getItem('access_role') === 'STAFF' || localStorage.getItem('access_role') === 'AGENT';
        return (
            <Fragment>
                <Row className="sidebar-custom">
                    <Column>
                        <Column className="p-20" >
                            <div>
                                <img src={ifadhaLogo} alt="#" width="70"/>
                                <span className=" sidebar-custom-title"><b>IFADHA</b></span>
                            </div>
                        </Column>
                        <Column className="sidebar-custom-separator"></Column>
                        <Column className="sidebar-custom-column" onClick={() => this.resetToggle()}>
                            <Link to={`${process.env.PUBLIC_URL}/home/dashboard`}>
                                <Home className="sidebar-custom-menu" />
                                <span id="dashboard" className="sidebar-custom-font">Dashboard</span>
                            </Link>
                        </Column>
                        <Column hidden={isStudent || isStaff} className="sidebar-custom-column" onClick={() => this.resetToggle()}>
                            <Link to={`${process.env.PUBLIC_URL}/home/subAgency`}>
                                <Airplay className="sidebar-custom-menu"/>
                                <span id="subAgency" className="sidebar-custom-font">Sub Agency</span>
                            </Link>
                        </Column>
                        <Column hidden={isStudent || isStaff} className="sidebar-custom-column">
                            <ul> 
                                <Users className="sidebar-custom-menu" style={{"cursor":"pointer"}}/>
                                <span id="users" className="sidebar-custom-font" style={{"cursor":"pointer"}}
                                    onClick={() => this.toggleUsers()}>Users
                                </span>
                                <li>
                                    <Column hidden={this.state.collapseUsers}  className="p-l-10 sidebar-custom-column">
                                        <Link to={`${process.env.PUBLIC_URL}/home/user/staff`}>
                                            <User className="sidebar-custom-menu"/>
                                            <span id="staff" className="sidebar-custom-font">Staff</span>
                                        </Link>
                                    </Column>
                                </li>
                                <li>
                                    <Column hidden={this.state.collapseUsers}  className="p-l-10 sidebar-custom-column">
                                        <Link to={`${process.env.PUBLIC_URL}/home/user/admin`}>
                                            <User className="sidebar-custom-menu"/>
                                            <span id="admin" className="sidebar-custom-font">Admin</span>
                                        </Link>
                                    </Column>
                                </li>
                            </ul>
                        </Column>
                        <Column hidden={isStudent} className="sidebar-custom-column" onClick={() => this.resetToggle()}>
                            <Link to={`${process.env.PUBLIC_URL}/home/user/student`}>
                                <User className="sidebar-custom-menu"/> 
                                <span id="student" className="sidebar-custom-font">Student</span>
                            </Link>
                        </Column>
                        <Column className="sidebar-custom-column" onClick={() => this.resetToggle()}>
                            <Link to={`${process.env.PUBLIC_URL}/home/application`}>
                                <Box className="sidebar-custom-menu"/> 
                                <span hidden={!isStudent} className="sidebar-custom-font">My Application</span>
                                <span hidden={isStudent} className="sidebar-custom-font">Applicant Form</span>
                            </Link>
                        </Column>
                        <Column hidden={isStudent || isStaff} className="sidebar-custom-column">
                            <ul> 
                                <Settings className="sidebar-custom-menu" style={{"cursor":"pointer"}}/>
                                <span id="settings" className="sidebar-custom-font" style={{"cursor":"pointer"}} onClick={() => this.toggleSettings()}>Settings</span>
                                <li>
                                    <Column hidden={this.state.collapseSettings} className="p-l-10 sidebar-custom-column">
                                        <Link to={`${process.env.PUBLIC_URL}/home/setting/staffMapping`}>
                                            <User className="sidebar-custom-menu"/>
                                            <span id="staffMapping" className="sidebar-custom-font">Staff Mapping</span>
                                        </Link>
                                    </Column>
                                </li>
                            </ul>
                        </Column>
                    </Column>
                </Row>
            </Fragment>
        );
    }

}

export default SidebarComponent;