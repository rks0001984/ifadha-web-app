import React, {Component} from 'react';
import axios from 'axios';
import PipelineDashboardColumn from './PipelineDashboardColumn';

class PipelineDashboard extends Component {
	constructor(props) {
		super(props);
		this.state = ({
			isLoading: true,
			projects: [],
			draggedOverCol: 0,
		});
		this.handleOnDragEnter = this.handleOnDragEnter.bind(this);
		this.handleOnDragEnd = this.handleOnDragEnd.bind(this);
		this.columns = [
			{name: 'Registered', stage: 1},
			{name: 'Document Recieved', stage: 2},
			{name: 'Submitted to University', stage: 3},
			{name: 'Conditioanl offer letter', stage: 4}
		];
	}
    
    componentDidMount() {
        const options = {
            headers: { 'access-token': localStorage.getItem('access_token')}
        };

        axios.get(process.env.REACT_APP_ENDPOINT + `applicant/pipelines`, options)
            .then(response => {
                if(response.status === 200){
                    this.setState({
                        //projects: response.data, 
                        projects: projectList,
                        isLoading: false 
                    })
                } else if(response.status === 1015) {
                    // TODO
                }            
            })
            .catch(error => {
                console.log(error)
            });

    }

	//this is called when a Kanban card is dragged over a column (called by column)
	handleOnDragEnter(e, stageValue) {
		this.setState({ draggedOverCol: stageValue });
	}

	//this is called when a Kanban card dropped over a column (called by card)
	handleOnDragEnd(e, project) {
		const updatedProjects = this.state.projects.slice(0);
		updatedProjects.find((projectObject) => {return projectObject.name === project.name;}).project_stage = this.state.draggedOverCol;
		this.setState({ projects: updatedProjects });
	}

	render() {
		if (this.state.isLoading) {
			return (<h3>Loading...</h3>);
		}

		return  (
            <div>
                {this.columns.map((column) => {
                    return (
                        <PipelineDashboardColumn
                            name={ column.name }
                            stage={ column.stage }
                            projects={ this.state.projects.filter((project) => {return parseInt(project.project_stage, 10) === column.stage;}) }
                            onDragEnter={ this.handleOnDragEnter }
                            onDragEnd={ this.handleOnDragEnd }
                            key={ column.stage }
                        />
                    );
                })}
            </div>
		);
	}
}

/*
 * Projects to be displayed on Kanban Board
 */
let projectList = [
    {
      name: 'Project 1',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam posuere dui vel urna egestas rutrum. ',
      project_stage: 1
    },
    {
      name: 'Project 2',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam posuere dui vel urna egestas rutrum. ',
      project_stage: 1
    },
    {
      name: 'Project 3',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam posuere dui vel urna egestas rutrum. ',
      project_stage: 1
    },
    {
      name: 'Project 4',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam posuere dui vel urna egestas rutrum. ',
      project_stage: 2
    },
    {
      name: 'Project 5',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam posuere dui vel urna egestas rutrum. ',
      project_stage: 3
    },
    {
      name: 'Project 6',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam posuere dui vel urna egestas rutrum. ',
      project_stage: 3
    },
    {
      name: 'Project 7',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam posuere dui vel urna egestas rutrum. ',
      project_stage: 4
    },
  ];

export default PipelineDashboard;